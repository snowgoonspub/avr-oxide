#![no_std]
#![no_main]

#![feature(asm_experimental_arch)]

mod devices;

use core::cell::RefCell;


use avr_oxide::alloc::boxed::Box;
use avr_oxide::println;
use avr_oxide::devices::{OxideSerialPort, OxideSerialBus};
use avr_oxide::devices::serialport::SerialOptions;
use avr_oxide::devices::button::ButtonState;
use avr_oxide::devices::masterclock::{DelayEvents, TickEvents};
use avr_oxide::hal::generic::serial::{BaudRate, DataBits, Parity, SerialPortMode, StopBits};
use avr_oxide::hal::generic::twi::TwiAddr;
use avr_oxide::util::persist::derive::Persist;
use avr_oxide::time::Duration;
use avr_oxide::boards::board;
use avr_oxide::hal::generic::watchdog::WatchdogPeriod;
use avr_oxide::hal::generic::watchdog::WatchdogController;
use avr_oxide::thread;
use avrox_storage::serprom;
use avr_oxide::devices::serialbus::UsesSerialBusClient;
use avrox_display::displays;
use avrox_display::gfx::pixels::{Monochromatic, Grey};
use avrox_display::gfx::primitives::{position, ConstScaleUp, HorizontalPair, Overlay};
use avrox_display::gfx::sevenseg::SevenSegmentDisplay;
use avrox_display::gfx::fills::SolidFill;
use avr_oxide::{ StaticWrap, StaticRef };
use avrox_display::{DisplayDevice,PowerLevel};
use avrox_storage::fs::{ File, SnafusFileSystem, FileUid };
use avrox_storage::PageBuffer;
use avrox_display::gfx::img::ImageFile;
use avr_oxide::devices::nc::NC;
use avr_oxide::hardware;
use avrox_display::gfx::Renderable;
use avrox_display::drivers::solomon::SolomonDisplay;
use avr_oxide::devices::serialbus::SerialBusClientImpl;
use avr_oxide::oxide::OxideSupervisor;
use avrox_display::drivers::solomon::GenericConfig;
use avrox_display::gfx::img::{MonochromeImage,GreyscaleImage};
use crate::devices::MyDevices;

// Code ======================================================================

// Filenames for our test image files
pub const ID_AVR_LOGO: FileUid = FileUid::with_id_c(1);
pub const ID_RUST_LOGO: FileUid = FileUid::with_id_c(2);
pub const ID_AVROXIDE_LOGO: FileUid = FileUid::with_id_c(3);


/**
 * Main function demonstrating the simple on_event based approach to handling
 * device IO.  This approach requires one of the allocator features to be
 * enabled.
 */
#[avr_oxide::main(chip="atmega4809",stacksize=1024)]
pub fn main() -> ! {
  let supervisor = avr_oxide::oxide::instance();
  MyDevices::init();


  // Configure the serial port early so we can get any panic!() messages
  let mut serial= StaticWrap::new(OxideSerialPort::using_port_and_pins(board::usb_serial(),
                                                                       board::usb_serial_pins().0,
                                                                       board::usb_serial_pins().1).mode(SerialPortMode::Asynch(BaudRate::Baud9600, DataBits::Bits8, Parity::None, StopBits::Bits1)));



  let mut options = SerialOptions::default();
  options.set_interactive(true);
  options.set_lf_to_crlf(true);
  options.set_echo(true);
  options.set_blocking_read(true);
  options.set_read_to_eol(true);
  serial.borrow_mut().set_options(options);
  avr_oxide::stdout::set_stdio(serial.borrow_mut());

  println!("AVRoxide Display Test App (rustc nightly-2024-05-01)");

  let bus = &MyDevices::borrow().i2c_bus;
  supervisor.listen(bus);

  // Set up our storage drivers
  let storagebus = OxideSerialBus::client(bus, TwiAddr::addr(0xA0));
  let fs         = StaticWrap::new(SnafusFileSystem::with_driver(PageBuffer::<32,_>::with_driver(serprom::BR24T1M_3AM::using_client(storagebus))));

  // And also our display drivers

  #[cfg(feature="waterbotv2")]
    let display = {
    let displaybus = OxideSerialBus::client(bus, avrox_display::displays::displayvisions::DVEAW096016DriverConfig::DEFAULT_I2C);
    StaticWrap::new(displays::DisplayVisionsEAW096016::using_pin_and_client(board::pin_d(13), displaybus))
  };
  #[cfg(feature="xplained")]
    let display = {
    let displaybus = OxideSerialBus::client(bus, avrox_display::displays::adafruit::Greyscale128x128DriverConfig::DEFAULT_I2C);
    StaticWrap::new(displays::AdafruitGreyscale128x128::using_pin_and_client(NC::new(), displaybus))
  };

  display.borrow().reset().unwrap();
  display.borrow().request_power_level(PowerLevel::Normal).unwrap();
  display.borrow().set_double_buffering(false).unwrap();

  // Now set up what we want to display
  static mut BIG_COUNTER : u32 = 0x00;
  let mut big_value = StaticWrap::new(RefCell::new(Some(0x00u32)));

  let counter = ConstScaleUp::<2,2,_>::new(SevenSegmentDisplay::<5,16,_,_>::new(Grey::GREY75PC, Grey::GREY25PC, unsafe {big_value.borrow().static_ref()}));

  if !fs.borrow().is_formatted().unwrap() {
    println!("Filesystem not formatted!");
    panic!();
  }

  let logo_file = File::open_on(unsafe { fs.borrow().static_ref() }, ID_AVROXIDE_LOGO).unwrap();

  #[cfg(feature="waterbotv2")]
  let logo = MonochromeImage::from(ImageFile::with_file(logo_file).unwrap());
  #[cfg(feature="xplained")]
  let logo = GreyscaleImage::from(ImageFile::with_file(logo_file).unwrap());


  let scene = Overlay::new(
    HorizontalPair::<_,_,position::Beginning>::new(logo, counter),
    SolidFill::new(Monochromatic::BLACK)
  );


  // So I can trigger the scope (debugging)
  let red_led = &MyDevices::borrow().red_led;
  red_led.set_on();
  red_led.set_off();

  unsafe { core::arch::asm!("break") };

  // First time round we'll render the whole scene, subsequent calls
  // will use render_changed() to only update dynamic parts
  display.borrow().render(&scene).unwrap();

  // Clock setup
  let master_clock = &MyDevices::borrow().master_clock;
  let wall_clock = &MyDevices::borrow().wall_clock;

  // Set an event handler to be called every time someone presses the button
  let left_button = &MyDevices::borrow().left_button;
  left_button.on_click(Box::new(move |_pinid, state|{
    let green_led = &MyDevices::borrow().green_led;
    green_led.toggle();
    match state {
      ButtonState::Pressed => {
        avr_oxide::util::debug::print_thread_state();
      },
      _ => {}
    }
  }));


  // Print the time every time someone clicks the blue button
  let down_button = &MyDevices::borrow().down_button;
  down_button.on_click(Box::new(move |_pinid, state|{
    match state {
      ButtonState::Pressed => {
        println!("Total runtime: {:?} seconds", wall_clock.runtime());
      },
      _ => {}
    }
  }));


  // An event handler every time the master clock ticks
  master_clock.on_tick(Box::new(move |_timerid, _duration|{
    unsafe { BIG_COUNTER += 1; }
  }));


  // And another one for the once-a-second wallclock ticks
  {
    let display = display.borrow();

    wall_clock.on_tick(Box::new(move |_timerid, _duration|{
      let blue_led = &MyDevices::borrow().blue_led;

      blue_led.toggle();
    }));
  }


  // Display power saving.  We'll launch a timer that turns the display
  // off after a certain time
  {
    let display = display.borrow();

    wall_clock.after_delay(Duration::from_secs(10), Box::new(move|_timerid|{
      //display.request_power_level(PowerLevel::Reduced).unwrap();
    }));
  }


  // But we'll turn it back on again whenever the yellow button is pressed
  {
    let display = display.borrow();
    let right_button = &MyDevices::borrow().right_button;

    right_button.on_click(Box::new(move |_pinid, state|{
      match state {
        ButtonState::Pressed => {
          display.request_power_level(PowerLevel::Normal).unwrap();
          // Refresh the display
          big_value.borrow_mut().replace(Some(unsafe { BIG_COUNTER }));
          display.render_changed(&scene).unwrap();
        },
        _ => {
          let display = StaticRef::clone(&display);
          wall_clock.after_delay(Duration::from_secs(10), Box::new(move|_timerid|{
            display.request_power_level(PowerLevel::Reduced).unwrap();
          }));
        }
      }
    }));
  }

/*

  // Let's show a different style of programming - instead of the
  // event-oriented style we've been used to, let's use a threaded style
  // with blocking I/O methods
  #[cfg(feature="waterbotv2")]
    let white_led = StaticWrap::new(OxideLed::with_pin(board::pin_d(10)));
  #[cfg(feature="xplained")]
    let white_led = StaticWrap::new(OxideLed::with_pin(hardware::port::portb::pin(1)));

  {
    let white_led  = white_led.borrow();

    let _jh = thread::Builder::new().stack_size(192).spawn(move ||{
      let up_button = &MyDevices::borrow().up_button;

      loop {
        match up_button.wait_for_change() {
          ButtonState::Pressed => {
            white_led.set_on();
            wall_clock.wait(Duration::from_millis(3000));
            white_led.set_off();
          },
          _ => {
          }
        }
      }
    });
  }

  */

  // Tell the supervisor what devices to pay attention to, and then enter
  // the main loop.
  supervisor.listen(serial.borrow());

  MyDevices::listen_all();

  // Enable the watchdog
  let wdt = avr_oxide::hardware::watchdog::instance();
  //wdt.enable(WatchdogPeriod::Off, WatchdogPeriod::Millis4096);

  supervisor.run_with_prehandler(|_event|{
    wdt.kick();
    true
  });
}

