/* devices.rs
 *
 * Developed by Tim Walls <tim.walls@snowgoons.com>
 * Copyright (c) All Rights Reserved, Tim Walls
 */
//! Doc comment for this file
//!

// Imports ===================================================================

use core::mem::MaybeUninit;
use avr_oxide::boards::board;
use avr_oxide::devices::{OxideButton, OxideLed, OxideMasterClock, OxideSerialBus, OxideWallClock, UsesPin};
use avr_oxide::devices::debouncer::Debouncer;
use avr_oxide::devices::inverter::Inverter;
use avr_oxide::devices::nc::NC;
use avr_oxide::hal::atmega4809::nvm;
use avr_oxide::hal::generic::twi::{InterfaceMode, PortSpeed};
use avr_oxide::hardware;

// Declarations ==============================================================
pub struct MyDevices
{
  pub i2c_bus: OxideSerialBus<'static>,

  pub master_clock: OxideMasterClock<'static,'static, hardware::timer::tcb0::TimerImpl>,
  pub wall_clock:   OxideWallClock<'static,'static,hardware::timer::rtc::TimerImpl>,

  pub green_led:  OxideLed,
  pub yellow_led: OxideLed,
  pub red_led:    OxideLed,
  pub blue_led:   OxideLed,

  pub up_button:    OxideButton<'static,'static>,
  pub left_button:  OxideButton<'static,'static>,
  pub right_button: OxideButton<'static,'static>,
  pub down_button:  OxideButton<'static,'static>
}

static mut DEVICES : MaybeUninit<MyDevices> = MaybeUninit::uninit();

// Code ======================================================================
impl Default for MyDevices {
  fn default() -> Self {
    MyDevices {
      i2c_bus: OxideSerialBus::using_bus(hardware::twi::twi0::instance().mux(hardware::twi::twi0::TwiPins::MasterASlaveC).mode(InterfaceMode::I2C, PortSpeed::Standard)),

      master_clock: OxideMasterClock::with_timer::<20>(hardware::timer::tcb0::instance()),
      wall_clock:   OxideWallClock::with_timer(hardware::timer::rtc::instance()),

      #[cfg(feature="waterbotv2")]
      green_led:  OxideLed::with_pin(board::pin_d(7)),
      #[cfg(feature="waterbotv2")]
      yellow_led: OxideLed::with_pin(board::pin_d(8)),
      #[cfg(feature="waterbotv2")]
      red_led:    OxideLed::with_pin(board::pin_d(11)),
      #[cfg(feature="waterbotv2")]
      blue_led:   OxideLed::with_pin(board::pin_d(12)),

      #[cfg(feature="xplained")]
      green_led:  OxideLed::with_pin(hardware::port::porta::pin(1)),
      #[cfg(feature="xplained")]
      yellow_led: OxideLed::with_pin(hardware::port::porte::pin(3)),
      #[cfg(feature="xplained")]
      red_led:    OxideLed::with_pin(hardware::port::porte::pin(0)),
      #[cfg(feature="xplained")]
      blue_led:   OxideLed::with_pin(hardware::port::porte::pin(1)),


      #[cfg(feature="waterbotv2")]
      up_button:     OxideButton::using(Inverter::using(Debouncer::with_pin(board::pin_a(6)))),
      #[cfg(feature="waterbotv2")]
      left_button:   OxideButton::using(Inverter::using(Debouncer::with_pin(board::pin_a(2)))),
      #[cfg(feature="waterbotv2")]
      right_button:  OxideButton::using(Inverter::using(Debouncer::with_pin(board::pin_a(3)))),
      #[cfg(feature="waterbotv2")]
      down_button:   OxideButton::using(Inverter::using(Debouncer::with_pin(board::pin_a(7)))),

      #[cfg(feature="xplained")]
      up_button:     OxideButton::using(NC::new()),
      #[cfg(feature="xplained")]
      left_button:   OxideButton::using(Inverter::using(Debouncer::with_pin(board::sw0()))),
      #[cfg(feature="xplained")]
      right_button:  OxideButton::using(NC::new()),
      #[cfg(feature="xplained")]
      down_button:   OxideButton::using(Inverter::using(Debouncer::with_pin(board::sw1()))),
    }
  }
}


impl MyDevices {
  pub fn init() {
    unsafe {
      DEVICES.write(MyDevices::default());
    }
  }

  pub fn borrow() -> &'static MyDevices {
    unsafe {
      & *DEVICES.as_ptr()
    }
  }

  pub fn listen_all() {
    let supervisor = avr_oxide::oxide::instance();

    supervisor.listen(&Self::borrow().i2c_bus);
    supervisor.listen(&Self::borrow().up_button);
    supervisor.listen(&Self::borrow().left_button);
    supervisor.listen(&Self::borrow().right_button);
    supervisor.listen(&Self::borrow().down_button);
    supervisor.listen(&Self::borrow().wall_clock);
    supervisor.listen(&Self::borrow().master_clock);
  }
}

// Tests =====================================================================
