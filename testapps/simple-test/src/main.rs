#![no_std]
#![no_main]

#![feature(asm_experimental_arch)]

mod devices;

use avr_oxide::alloc::vec::Vec;
use avr_oxide::alloc::boxed::Box;
use avr_oxide::println;
use avr_oxide::devices::{UsesPin, OxideLed, OxideSerialPort };
use avr_oxide::devices::serialport::SerialOptions;
use avr_oxide::devices::button::ButtonState;
use avr_oxide::devices::masterclock::TickEvents;
use avr_oxide::hal::generic::serial::{BaudRate, DataBits, Parity, SerialPortMode, StopBits};

use avr_oxide::util::persist::derive::Persist;
use avr_oxide::time::Duration;
use avr_oxide::boards::board;
use avr_oxide::hal::generic::watchdog::WatchdogPeriod;
use avr_oxide::hal::generic::watchdog::WatchdogController;
use avr_oxide::thread;






use avr_oxide::StaticWrap;




use avr_oxide::hardware;
use avr_oxide::util::persist::Persist;
use avr_oxide::hal::generic::eeprom::EepromSpace;

use crate::devices::MyDevices;

// Code ======================================================================
#[derive(Persist)]
#[persist(magicnumber = 2)]
struct MyPersistentData {
  number_of_boots: u16,
  username: Option<Vec<u8>>
}

impl Default for MyPersistentData {
  fn default() -> Self {
    MyPersistentData {
      number_of_boots: 0,
      username: Option::None
    }
  }
}

/**
 * Main function demonstrating the simple on_event based approach to handling
 * device IO.  This approach requires one of the allocator features to be
 * enabled.
 */
#[avr_oxide::main(chip="atmega4809",stacksize=768)]
pub fn main() -> ! {
  let supervisor = avr_oxide::oxide::instance();
  MyDevices::init();


  // Configure the serial port early so we can get any panic!() messages
  let mut serial= StaticWrap::new(OxideSerialPort::using_port_and_pins(board::usb_serial(),
                                                                       board::usb_serial_pins().0,
                                                                       board::usb_serial_pins().1).mode(SerialPortMode::Asynch(BaudRate::Baud9600, DataBits::Bits8, Parity::None, StopBits::Bits1)));



  let mut options = SerialOptions::default();
  options.set_interactive(true);
  options.set_lf_to_crlf(true);
  options.set_echo(true);
  options.set_blocking_read(true);
  options.set_read_to_eol(true);
  serial.borrow_mut().set_options(options);
  avr_oxide::stdout::set_stdio(serial.borrow_mut());

  println!("AVRoxide Simple Test App...");

  shared::test_32bit_maths().unwrap();
  shared::test_memory_allocator().unwrap();

  // Launch a background thread to talk to the user on the serial port
/*  {
    let mut serial = serial.borrow();
    let _jh =
      avr_oxide::concurrency::thread::Builder::new().stack_size(256).spawn(move ||{

        // Load and save from EEPROM
        let eeprom = &mut hardware::nvm::eeprom::instance();
        let mut my_data : MyPersistentData = Persist::load_with_or_default(eeprom.reader());

        if my_data.number_of_boots == 0 {
          println!("It's the first time I've been booted!\n\n");
        } else {
          println!("I have been booted {} times before.\n\n", my_data.number_of_boots);
        }
        my_data.number_of_boots += 1;

        // Reading from the serial port
        let name = shared::read_name_from_stdin();


        match my_data.username {
          Some(oldname) => {
            println!("\n\nHmm.  The last guy was {}, but hello {} - nice to meet you!",
                     core::str::from_utf8(oldname.as_slice()).unwrap(),
                     core::str::from_utf8(name.as_slice()).unwrap());
          },
          None => {
            println!("\n\nWelcome to AVRoxide, {}!\n\n",
                     core::str::from_utf8(name.as_slice()).unwrap());
          }
        }

        my_data.username = Some(name);

        my_data.save_with(eeprom.writer()).unwrap();

        123
      });
  }*/

  // Clock setup
  let master_clock = &MyDevices::borrow().master_clock;
  let wall_clock = &MyDevices::borrow().wall_clock;

  // Set an event handler to be called every time someone presses the button
  let left_button = &MyDevices::borrow().left_button;
  left_button.on_click(Box::new(move |_pinid, state|{
    let green_led = &MyDevices::borrow().green_led;
    green_led.toggle();
    match state {
      ButtonState::Pressed => {
        avr_oxide::util::debug::print_thread_state();
      },
      _ => {}
    }
  }));


  // Print the time every time someone clicks the blue button
  let down_button = &MyDevices::borrow().down_button;
  down_button.on_click(Box::new(move |_pinid, state|{
    match state {
      ButtonState::Pressed => {
        println!("Total runtime: {:?} seconds", wall_clock.runtime());
      },
      _ => {}
    }
  }));


  // An event handler every time the master clock ticks
  master_clock.on_tick(Box::new(move |_timerid, _duration|{
    let red_led = &MyDevices::borrow().red_led;
    red_led.toggle();
  }));


  // And another one for the once-a-second wallclock ticks
  {
    wall_clock.on_tick(Box::new(move |_timerid, _duration|{
      let blue_led = &MyDevices::borrow().blue_led;

      blue_led.toggle();
    }));
  }


  // Let's show a different style of programming - instead of the
  // event-oriented style we've been used to, let's use a threaded style
  // with blocking I/O methods
  #[cfg(feature="waterbotv2")]
    let white_led = StaticWrap::new(OxideLed::with_pin(board::pin_d(10)));
  #[cfg(feature="xplained")]
    let white_led = StaticWrap::new(OxideLed::with_pin(hardware::port::portb::pin(1)));

  {
    let white_led  = white_led.borrow();

    let _jh = thread::Builder::new().stack_size(256).spawn(move ||{
      let up_button = &MyDevices::borrow().up_button;

      loop {
        match up_button.wait_for_change() {
          ButtonState::Pressed => {
            white_led.set_on();
            wall_clock.wait(Duration::from_millis(3000));
            white_led.set_off();
          },
          _ => {
          }
        }
      }
    });
  }

  // Tell the supervisor what devices to pay attention to, and then enter
  // the main loop.
  supervisor.listen(serial.borrow());

  MyDevices::listen_all();

  // Enable the watchdog
  let wdt = avr_oxide::hardware::watchdog::instance();
  //wdt.enable(WatchdogPeriod::Off, WatchdogPeriod::Millis4096);

  supervisor.run_with_prehandler(|_event|{
    wdt.kick();
    true
  });
}

