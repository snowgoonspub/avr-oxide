#![no_std]
#![no_main]

#![feature(asm_experimental_arch)]
mod testdata;

use core::borrow::Borrow;
use avr_oxide::println;
use avr_oxide::devices::{UsesPin, OxideLed, OxideSerialPort, OxideSerialBus, OxideMasterClock};
use avr_oxide::devices::serialport::SerialOptions;
use avr_oxide::hal::generic::serial::{BaudRate, DataBits, Parity, SerialPortMode, StopBits};
use avr_oxide::hal::generic::twi::{TwiAddr, InterfaceMode, PortSpeed};
use avr_oxide::boards::board;
use avr_oxide::hardware;
use avrox_storage::serprom;
use avr_oxide::devices::serialbus::UsesSerialBusClient;
use avr_oxide::StaticWrap;
use avrox_storage::fs::{File,SnafusFileSystem,Write,Read};
use avrox_storage::PageBuffer;
use avr_oxide::devices::button::ButtonState;
use avr_oxide::devices::OxideButton;
use avr_oxide::devices::inverter::Inverter;
use avr_oxide::devices::debouncer::Debouncer;
use avr_oxide::alloc::boxed::Box;


// Code ======================================================================

/**
 * Main function demonstrating the simple on_event based approach to handling
 * device IO.  This approach requires one of the allocator features to be
 * enabled.
 */
#[avr_oxide::main(chip="atmega4809",stacksize=1024)]
pub fn main() -> ! {
  let supervisor = avr_oxide::oxide::instance();

  // Configure the serial port early so we can get any panic!() messages
  let mut serial= StaticWrap::new(OxideSerialPort::using_port_and_pins(board::usb_serial(),
                                                                       board::usb_serial_pins().0,
                                                                       board::usb_serial_pins().1).mode(SerialPortMode::Asynch(BaudRate::Baud9600, DataBits::Bits8, Parity::None, StopBits::Bits1)));



  let mut options = SerialOptions::default();
  options.set_interactive(true);
  options.set_lf_to_crlf(true);
  options.set_echo(true);
  options.set_blocking_read(true);
  options.set_read_to_eol(true);
  serial.borrow_mut().set_options(options);
  avr_oxide::stdout::set_stdio(serial.borrow_mut());

  let master_clock = StaticWrap::new(OxideMasterClock::with_timer::<20>(hardware::timer::tcb0::instance()));
  supervisor.listen(master_clock.borrow());


  #[cfg(feature="arduino")]
  {
    println!("Data loader:");
    println!("  RED:   Format drive");
    println!("  BLUE:  Write data");
    println!("  GREEN: Validate data");
  }
  #[cfg(feature="xplained")]
  {
    println!("Data loader:");
    println!("  SW0: Format drive");
    #[cfg(feature="write")]
    println!("  SW1: Write data");
    #[cfg(feature="validate")]
    println!("  SW1: Validate data");
  }
  // Buttons and LEDs setup
  #[cfg(feature="arduino")]
  let ( green_led, red_led ) = (
    StaticWrap::new(OxideLed::with_pin(board::pin_d(7))),
    StaticWrap::new(OxideLed::with_pin(board::pin_d(11)))
  );

  #[cfg(feature="xplained")]
    let ( green_led, red_led ) = (
    StaticWrap::new(OxideLed::with_pin(hardware::port::porta::pin(1))),
    StaticWrap::new(OxideLed::with_pin(hardware::port::porte::pin(3)))
  );

  #[cfg(feature="arduino")]
  let ( format_button, write_button, validate_button ) = (
    StaticWrap::new(OxideButton::using(Inverter::using(Debouncer::with_pin(board::pin_a(6))))),
    StaticWrap::new(OxideButton::using(Inverter::using(Debouncer::with_pin(board::pin_a(7))))),
    StaticWrap::new(OxideButton::using(Inverter::using(Debouncer::with_pin(board::pin_a(2)))))
  );

  #[cfg(all(feature="xplained",feature="write"))]
  let (format_button, write_button) = (
    StaticWrap::new(OxideButton::using(Inverter::using(Debouncer::with_pin(board::sw0())))),
    StaticWrap::new(OxideButton::using(Inverter::using(Debouncer::with_pin(board::sw1())))),
  );

  #[cfg(all(feature="xplained",feature="validate"))]
    let (format_button, validate_button) = (
    StaticWrap::new(OxideButton::using(Inverter::using(Debouncer::with_pin(board::sw0())))),
    StaticWrap::new(OxideButton::using(Inverter::using(Debouncer::with_pin(board::sw1())))),
  );


  let bus = StaticWrap::new(OxideSerialBus::using_bus(hardware::twi::twi0::instance().mux(hardware::twi::twi0::TwiPins::MasterASlaveC).mode(InterfaceMode::I2C, PortSpeed::Fast)));
  supervisor.listen(bus.borrow());
  let bus_client = OxideSerialBus::client(bus.borrow(), TwiAddr::addr(0xA0));
  let storage    = serprom::BR24T1M_3AM::using_client(bus_client);
  let mut fs     = StaticWrap::new(SnafusFileSystem::with_driver(PageBuffer::<32,_>::with_driver(storage)));

  {
    let red_button = format_button.borrow();
    let fs = fs.borrow();
    let green_led = green_led.borrow();
    let red_led = red_led.borrow();

    red_button.borrow().on_click(Box::new(move |_pinid, state| {
      if state == ButtonState::Released {
        // Red means don't reset me, I might be writing stuff
        green_led.set_off();
        red_led.set_on();

        // OK!  Let's do what we're here for

        println!("Formatting...");
        fs.format().unwrap();
        println!("Done.");


        // We're finished.
        green_led.set_on();
        red_led.set_off();
      }
    }));
  }

  #[cfg(feature="write")]
  {
    let blue_button = write_button.borrow();
    let fs = unsafe { fs.borrow().static_ref() };
    let green_led = green_led.borrow();
    let red_led = red_led.borrow();

    blue_button.borrow().on_click(Box::new(move |_pinid, state| {
      if state == ButtonState::Released {
        // Red means don't reset me, I might be writing stuff
        green_led.set_off();
        red_led.set_on();

        println!("Writing AVR logo file @ {:?}", testdata::ID_AVR_LOGO);
        let mut file = File::create_on(fs, testdata::ID_AVR_LOGO).unwrap();
        file.write_all(&testdata::DATA_AVR_LOGO).unwrap();
        file.sync_all().unwrap();

        println!("Writing Rust logo file @ {:?}", testdata::ID_RUST_LOGO);
        let mut file = File::create_on(fs, testdata::ID_RUST_LOGO).unwrap();
        file.write_all(&testdata::DATA_RUST_LOGO).unwrap();
        file.sync_all().unwrap();

        println!("Writing AVRoxide logo file @ {:?}", testdata::ID_AVROXIDE_LOGO);
        let mut file = File::create_on(fs, testdata::ID_AVROXIDE_LOGO).unwrap();
        file.write_all(&testdata::DATA_AVROXIDE_LOGO).unwrap();
        file.sync_all().unwrap();


        println!("Finished!");

        // We're finished.
        green_led.set_on();
        red_led.set_off();
      }
    }));
  }

  #[cfg(feature="validate")]
  {
    let fs = unsafe { fs.borrow().static_ref() };
    let green_button = validate_button.borrow();

    green_button.borrow().on_click(Box::new(move |_pinid, state| {
      if state == ButtonState::Released {
        println!("Testing AVR logo file");
        let mut file = File::open_on(fs, testdata::ID_AVR_LOGO).unwrap();
        let mut buffer = [0x00u8; 145];
        file.read_exact(&mut buffer).unwrap();

        if buffer == testdata::DATA_AVR_LOGO {
          println!("  Data checked OK");
        } else {
          println!("  Data readback failed");
        }

        println!("Testing Rust logo file");
        let mut file = File::open_on(fs, testdata::ID_RUST_LOGO).unwrap();
        let mut buffer = [0x00u8; 121];
        file.read_exact(&mut buffer).unwrap();

        if buffer == testdata::DATA_RUST_LOGO {
          println!("  Data checked OK");
        } else {
          println!("  Data readback failed");
        }

        println!("Finished!");
      }
    }));
  }

  supervisor.listen(format_button.borrow());
  #[cfg(feature="write")]
  supervisor.listen(write_button.borrow());
  #[cfg(feature="validate")]
  supervisor.listen(validate_button.borrow());
  supervisor.listen(serial.borrow());
  supervisor.run()
}

