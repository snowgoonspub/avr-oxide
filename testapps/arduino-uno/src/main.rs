#![no_std]
#![no_main]

#![feature(asm_experimental_arch)]

use avr_oxide::alloc::vec::Vec;
use avr_oxide::alloc::boxed::Box;
use avr_oxide::{panic_if_err, print, println, pie};
use avr_oxide::devices::debouncer::Debouncer;
use avr_oxide::devices::{UsesPin, OxideMasterClock, OxideLed, OxideButton, OxideSerialPort, OxideWallClock};
use avr_oxide::devices::button::ButtonState;
use avr_oxide::devices::serialport::SerialOptions;
use avr_oxide::devices::masterclock::{DelayEvents, TickEvents};
use avr_oxide::hal::generic::eeprom::EepromSpace;
use avr_oxide::hal::generic::serial::{BaudRate, DataBits, Parity, SerialPortMode, StopBits};
use avr_oxide::io::{IoError, Read};
use avr_oxide::util::persist::derive::Persist;
use avr_oxide::util::persist::Persist;
use avr_oxide::time::Duration;
use avr_oxide::io::Write;
use avr_oxide::boards::board;
use core::arch::asm;
use avr_oxide::StaticWrap;

// Code ======================================================================

/**
 * Main function demonstrating the simple on_event based approach to handling
 * device IO.  This approach requires one of the allocator features to be
 * enabled.
 */
#[avr_oxide::main(chip="atmega328p")]
pub fn main() -> ! {
  let supervisor = avr_oxide::oxide::instance();

  // Configure the serial port early so we can get any panic!() messages
  let mut serial = StaticWrap::new(OxideSerialPort::using_port_and_pins(board::usb_serial(),
                                                                        board::usb_serial_pins().0,
                                                                        board::usb_serial_pins().1).mode(SerialPortMode::Asynch(BaudRate::Baud9600, DataBits::Bits8, Parity::None, StopBits::Bits1)));

  let mut options = SerialOptions::default();
  options.set_interactive(true);
  options.set_lf_to_crlf(true);
  options.set_echo(true);
  serial.set_options(options);

  //serial.write(b"ATmega328P Test Application\n");

  avr_oxide::stdout::set_stdout_wrapped(&mut serial);
  println!("ATmega328p Test Application...");


  // Setup LEDs
  let white_led = OxideLed::with_pin(board::pin_d(6));
  let green_led = OxideLed::with_pin(board::pin_d(5));
  let yellow_led = OxideLed::with_pin(board::pin_d(4));
  let red_led = OxideLed::with_pin(board::pin_d(3));
  let blue_led = OxideLed::with_pin(board::pin_d(2));

  // Setup Buttons

  let green_button = StaticWrap::new(OxideButton::using(Debouncer::with_pin(board::pin_d(8))));
  //let yellow_button = Handle::new(OxideButton::using(Debouncer::with_pin(arduino.d9)));
  //let blue_button = Handle::new(OxideButton::using(Debouncer::with_pin(arduino.d10)));
  //let red_button = Handle::new(OxideButton::using(Debouncer::with_pin(arduino.d11)));

  test_32bit_maths();

  // Set an event handler to be called every time someone presses the button
  green_button.on_click(Box::new(move |_pinid, _state|{
    green_led.toggle();
  }));

  // Tell the supervisor what devices to pay attention to, and then enter
  // the main loop.
  supervisor.listen(green_button.static_ref());

  supervisor.run();
}

fn test_32bit_maths() {
  println!("32bit Maths Tests:");

  let mut vol_a: u32 = 0;
  let mut vol_b: u32 = 0;

  let mut stackguard: u32 = 0;
  unsafe { core::ptr::write_volatile(&mut stackguard, 0xdeadbeef) };

  const ADD_TESTS : [(u32, u32, u32); 4] = [
    (1          ,1           , 2         ),
    (123        ,48123       , 48246     ),
    (58962      ,13189       , 72151     ),
    (128192     ,64956       , 193148    ),
  ];

  for (first, second, result) in ADD_TESTS {
    let mut put_on_the_stack: u16 = 0;
    unsafe { core::ptr::write_volatile(&mut put_on_the_stack, 0xf00f) };

    unsafe {
      core::ptr::write_volatile(&mut vol_a, first);
      core::ptr::write_volatile(&mut vol_b, second);

      let my_result = core::ptr::read_volatile(&vol_a) + core::ptr::read_volatile(&vol_b);

      if my_result != result {
        println!("Error: {} should not equal {}", my_result, result);
      }
    }
  }


  const MUL_TESTS : [(u32, u32, u32); 5] = [
    (1          , 1           , 1          ),
    (1          , 0           , 0          ),
    (2          , 48137       , 96274      ),
    (4          , 256         , 1024       ),
    (193148     , 8137        , 1571645276 ),
  ];

  for (first, second, result) in MUL_TESTS {
    let mut put_on_the_stack: u16 = 0;
    unsafe { core::ptr::write_volatile(&mut put_on_the_stack, 0xf00f) };

    unsafe {
      core::ptr::write_volatile(&mut vol_a, first);
      core::ptr::write_volatile(&mut vol_b, second);

      let my_result = core::ptr::read_volatile(&vol_a) * core::ptr::read_volatile(&vol_b);

      if my_result != result {
        panic!();
      }
    }
  }


  const DIV_TESTS : [(u32, u32, u32); 5] = [
    (1          , 1           , 1          ),
    (100        , 10          , 10         ),
    (512        , 4           , 128        ),
    (1571645276 , 17          , 92449722   ),
    (3295478013 , 539         , 6114059    ),
  ];

  for (first, second, result) in DIV_TESTS {
    let mut put_on_the_stack: u16 = 0;
    unsafe { core::ptr::write_volatile(&mut put_on_the_stack, 0xf00f) };

    unsafe {
      core::ptr::write_volatile(&mut vol_a, first);
      core::ptr::write_volatile(&mut vol_b, second);

      let my_result = core::ptr::read_volatile(&vol_a) / core::ptr::read_volatile(&vol_b);

      if my_result != result {
        panic!();
      }
    }
  }

  let stackguard_after = unsafe { core::ptr::read_volatile(&stackguard) };

  if stackguard_after != 0xdeadbeef {
    panic!();
  }

  println!("Completed OK");
}