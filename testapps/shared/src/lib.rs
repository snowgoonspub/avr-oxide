#![no_std]
#![no_main]

use avr_oxide::alloc::vec::Vec;
use avr_oxide::{OxideResult, print, println, thread};
use avr_oxide::devices::led::Led;
use avr_oxide::io::{IoError, Read};
use avr_oxide::sync::{Arc, Mutex};
//use avr_oxide::OxideResult::{Err,Ok};


pub fn thread_duel(pin1: &'static Led, pin2: &'static Led){
  let mutex = Arc::new(Mutex::new(()));

  let t_mutex = mutex.clone();
  let _thread1 = thread::Builder::new().stack_size(196).spawn(move || {
    loop {
      let _lock = t_mutex.lock();
      pin1.toggle();

      print!(":");
    }
  });

  let t_mutex = mutex.clone();
  let _thread2 = thread::Builder::new().stack_size(196).spawn(move || {
    let mut counter : u16 = 0;

    loop {
      let _lock = t_mutex.lock();
      pin2.toggle();
      counter += 1;

      if counter > 637 {
        println!("\nThread B still alive");
        counter = 0;
      }
    }
  });
}


pub fn read_name_from_serial<S: Read>(port: &mut S) -> Vec<u8> {

  print!("Please enter your name: ");
  let mut name : Vec<u8> = Vec::new();
  loop {
    let mut buf = [ 0u8; 16 ];
    let mut done = false;

    match port.read(&mut buf) {
      OxideResult::Ok(bytes) => {
        for i in 0..bytes {
          if buf[i] == b'\r' {
            done = true;
            break;
          } else {
            name.push(buf[i] as u8);
          }
        }
      }
      OxideResult::Err(_e) => {
        println!("Error reading name!");
        panic!();
      }
    }
    if done {
      break;
    }
  }
  name
}


pub fn test_32bit_maths() -> OxideResult<(),IoError> {
  println!("32bit Maths Function test");

  let mut failed = false;
  let mut vol_a: u32 = 0;
  let mut vol_b: u32 = 0;

  let mut stackguard: u32 = 0;
  unsafe { core::ptr::write_volatile(&mut stackguard, 0xdeadbeef) };

  const ADD_TESTS : [(u32, u32, u32); 4] = [
    (1          ,1           , 2         ),
    (123        ,48123       , 48246     ),
    (58962      ,13189       , 72151     ),
    (128192     ,64956       , 193148    ),
  ];

  for (first, second, result) in ADD_TESTS {
    let mut put_on_the_stack: u16 = 0;
    unsafe { core::ptr::write_volatile(&mut put_on_the_stack, 0xf00f) };

    unsafe {
      core::ptr::write_volatile(&mut vol_a, first);
      core::ptr::write_volatile(&mut vol_b, second);

      let my_result = core::ptr::read_volatile(&vol_a) + core::ptr::read_volatile(&vol_b);

      if my_result != result {
        println!("Failure: {} + {} should not equal {} ", first, second, my_result);
        failed = true;
      }
    }
  }


  const MUL_TESTS : [(u32, u32, u32); 5] = [
    (1          , 1           , 1          ),
    (1          , 0           , 0          ),
    (2          , 48137       , 96274      ),
    (4          , 256         , 1024       ),
    (193148     , 8137        , 1571645276 ),
  ];

  for (first, second, result) in MUL_TESTS {
    let mut put_on_the_stack: u16 = 0;
    unsafe { core::ptr::write_volatile(&mut put_on_the_stack, 0xf00f) };

    unsafe {
      core::ptr::write_volatile(&mut vol_a, first);
      core::ptr::write_volatile(&mut vol_b, second);

      let my_result = core::ptr::read_volatile(&vol_a) * core::ptr::read_volatile(&vol_b);

      if my_result != result {
        println!("Failure: {} * {} should not equal {} ", first, second, my_result);
        failed = true;
      }
    }
  }


  const DIV_TESTS : [(u32, u32, u32); 5] = [
    (1          , 1           , 1          ),
    (100        , 10          , 10         ),
    (512        , 4           , 128        ),
    (1571645276 , 17          , 92449722   ),
    (3295478013 , 539         , 6114059    ),
  ];

  for (first, second, result) in DIV_TESTS {
    let mut put_on_the_stack: u16 = 0;
    unsafe { core::ptr::write_volatile(&mut put_on_the_stack, 0xf00f) };

    unsafe {
      core::ptr::write_volatile(&mut vol_a, first);
      core::ptr::write_volatile(&mut vol_b, second);

      let my_result = core::ptr::read_volatile(&vol_a) / core::ptr::read_volatile(&vol_b);

      if my_result != result {
        println!("Failure: {} / {} should not equal {} ", first, second, my_result);
        failed = true;
      }
    }
  }

  let stackguard_after = unsafe { core::ptr::read_volatile(&stackguard) };

  if stackguard_after != 0xdeadbeef {
    println!("Stack guard was corrupted");
    failed = true;
  }

  match failed {
    true => {
      println!("\nFailure.\n");
      panic!()
    }
    false => {
      println!("\nSuccess.\n");
    }
  }

  OxideResult::Ok(())
}

/**
 * Do a test of the memory allocation routines.  It's somewhat harder to do
 * this since the heap area is now dynamic...
 */
pub fn test_memory_allocator() -> OxideResult<(),IoError> {
  println!("Memory allocation test");

  let mut failed:bool = false;

  // The stepsize of 13 is a reasonable compromise in terms of how long it
  // takes to run a complete test.
  for blocksize in (16u16..128).step_by(13) {
    println!("Blocksize {}", blocksize);

    let mut blocks : Vec<Vec<u8>> = Vec::with_capacity(8);

    println!("  Allocating...");

    for i in 0..8 {
      let mut block = Vec::new();

      if block.try_reserve(blocksize as usize).is_ok(){
        for _j in 0u16..blocksize {
          block.push(0xde);
        }

        blocks.push(block);
      } else {
        println!("Out of memory at block {}", i);

        // We ran out of space to allocate blocks
        break;
      }
    }

    println!("  Validating");
    let mut i = 0;
    for block in blocks {
      for char in block {
        if char != 0xde {
          println!("    Block {} ** CORRUPTED **", i);
          failed = true;
          break;
        }
      }

      i += 1;
    }
  }
  match failed {
    true => {
      println!("Failure.");
      panic!()
    }
    false => {
      println!("Success.");
    }
  };

  OxideResult::Ok(())
}