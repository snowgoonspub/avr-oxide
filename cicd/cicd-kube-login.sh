#!/bin/sh

if [ -z ${K8S_CA} ]; then
  echo "K8S_CA must be set (filename of CA certificate)"
  exit -1
fi
if [ -z ${K8S_SERVER} ]; then
  echo "K8S_SERVER must be set (Kubernetes API endpoint)"
  exit -1
fi
if [ -z ${K8S_TOKEN} ]; then
  echo "K8S_TOKEN must be set (Service Account token)"
  exit -1
fi
if [ -z ${K8S_CLUSTER} ]; then
  echo "K8S_CLUSTER must be set (Cluster name)"
  exit -1
fi


K8S_USER=${K8S_USER:-gitlabci}
CONTEXT=${K8S_USER}-${K8S_CLUSTER}

echo "${K8S_CA}" > /tmp/k8s.ca
kubectl config set-cluster ${K8S_CLUSTER} --server=${K8S_SERVER} --embed-certs=true --certificate-authority=/tmp/k8s.ca
kubectl config set-credentials ${K8S_USER} --token=${K8S_TOKEN}
kubectl config set-context ${CONTEXT} --cluster=${K8S_CLUSTER}
kubectl config set-context ${CONTEXT} --user=${K8S_USER}
kubectl config use-context ${CONTEXT}
