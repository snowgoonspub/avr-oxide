To get the CA Certificate:

`kubectl get cm kube-root-ca.crt -o jsonpath="{['data']['ca\.crt']}"`

To create an access token

```
k -n kube-system create serviceaccount gitlabci
k create clusterrolebinding gitlabci-cluster-admin --clusterrole=cluster-admin --serviceaccount=kube-system:gitlabci
k apply -f k8s-cicd-token.yaml
k describe secret gitlabci-sa-token -n kube-system
```