# AVR-Oxide
An extremely simple Rust runtime for AVR microcontrollers.

For more information and documentation, see the [AVRoxide] site.

## Why?
AVRoxide was borne out of [my] frustration with attempting to develop for an
[Arduino Nano Every] microcontroller board using the Rust programming
language.

The Arduino Nano Every uses a different version of AVR microcontroller to
the other Arduino family of boards - the [ATmega4809].  While there are
HAL[^1] crates available for various Arduino boards, none of them works with
this microcontroller, and that's a shame - it's actually a super nice chip
and a good upgrade on the ones used in the 'traditional' Arduino boards.  So,
support for it in Rust would obviously be nice to have.

### How we got here
Naturally, the first thing I tried to do was to [adapt](https://snowgoons.ro/posts/2020-11-11-compiling-rust-for-arduino-nano-every-part-one/) 
some of [the existing](https://snowgoons.ro/posts/2021-08-11-rust-on-arduino-nano-every-part-two/)
HALs to work with the Nano Every.  But this turned out to be a little
trickier than I anticipated, and in any event I discovered there were some
things that I didn't like about those HALs.  This is in no way a reflection
on the quality of thoes HALs - much more a reflection on my Rust abilities -
but I found I wanted something a little simpler and easier to understand.

And so, the idea of putting together my own HAL come to mind.  But, I also
wanted to move a little higher up the pipeline - not just to abstract away
the hardware, but also to put in place some standard patterns for things
like a standard event loop that would hide some of the details of things like
interrupt handling from the application developer.

Thus, AVRoxide is born!  The Rusty runtime for AVR microcontrollers.

# Supported Microcontrollers
* ATmega 4809 (used on Arduino Nano Every)
* ATmega 328P (used on Arduino Uno, Arduino Pro Mini and Arduino Nano)

Note that '328P support is currently at a very, very early stage, with only
basic port pins and serial USARTs working.  This chip also brings a new set
of Rust/LLVM code generation bugs and challenges, and I haven't worked out
how to deal with them all yet.

# Documentation
Getting started guides are at [https://avroxi.de/categories/gettingstarted/](https://avroxi.de/categories/gettingstarted/)

# Status
> **This is a work in progress**
> 
> It currently is functional, with a whole bunch of [caveats] about the
> readiness of Rust on AVR generally, and usable to build [simple programs].
>
> The intention is to expand it beyond the minimal set of devices currently
> supported, in due course.
> 
> Now and forever - use at your own risk :-).
> 
> *THERE ARE HEISENBUGS*
> Particularly involving multithreading.  Since I have reached a point of
> total exasperation attempting to track them down, for the timebeing I'm
> going to worry about other things and hope I come across the solution
> while working on something else.  So, really really, don't use this for
> anything critical right now - consider it an adventure game :-).

# Rust Toolchain
AVRoxide is built using the `nightly` Rust toolchain for AVR.  Since
10th May 2022, you can use the regular `nightly` toolchain - thank you
to [Patryk27] for [getting it working](https://github.com/rust-lang/compiler-builtins/issues/400#issuecomment-1121983170)!

Update, 2024: Some recent changes in `nightly` are causing problems.  But the
relatively recent `2024-05-01` toolchain works fine, so that's what I'm
currently using.

A Docker image containing a known-working version of the toolchain is also
available [on Docker Hub](https://hub.docker.com/layers/avr-rustbuilder/snowgoons/avr-rustbuilder/).
This is the version used in my CICD to test the build; if you want to verify
you can pull the same version using:
```
docker pull snowgoons/avr-rustbuilder:nightly-2024-06-06
```

Note that despite the later image date, this image also has the working
`nightly-2024-05-01` toolchain and sources pre-installed.

[AVRoxide]: https://avroxi.de/
[Rust]: https://www.rust-lang.org/what/embedded
[ATmega]: https://www.microchip.com/
[AVR]: https://www.microchip.com/en-us/products/microcontrollers-and-microprocessors/8-bit-mcus/avr-mcus
[my]: https://snowgoons.ro/
[ATmega4809]: https://www.microchip.com/en-us/products/microcontrollers-and-microprocessors/8-bit-mcus/avr-mcus/atmega4809
[Arduino Nano Every]: https://docs.arduino.cc/hardware/nano-every
[Rustdocs]: https://pages.avroxi.de/
[BSD 3 Clause]: https://opensource.org/licenses/BSD-3-Clause
[Gitlab Repository]: https://gitlab.com/snowgoonspub/avr-oxide
[caveats]: https://avroxi.de/post/a-working-toolchain/
[simple programs]: https://avroxi.de/post/an-example-program/
[Patryk27]: https://github.com/Patryk27

[^1]: Hardware Abstraction Layer
