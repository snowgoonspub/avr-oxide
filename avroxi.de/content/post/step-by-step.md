---
title: "Step-by-Step Guide"
date: 2022-01-03
image: "images/post/steps.png"
author: "Tim Walls" # use capitalize
description: "From zero to a compiling/deployable application, step by step"
categories: ["gettingstarted"]
tags: ["avroxide", "how-to", "arduino"]
draft: false
weight: -50
---
This guide walks you through each of the stages/prerequisites you need to
get building and deploying an AVRoxide application on an Arduino Nano Every,
with its ATmega4809 processor.

Commentary on the *why* for each of these steps is elsewhere on this site,
so here we'll stick to the What rather than the various Whys.

# 1. Pre-requisites for building Rust
First, install the various pre-requisites for building Rust:

* gcc-avr binutils-avr avr-libc wget lsb-release software-properties-common
* cmake
* ninja-build

These are the Ubuntu Linux package names - you will need equivalents for other
operating systems, e.g. use `homebrew` to find the various apps for Mac.

# 2. Build a working Rust for AVR toolchain
```
git clone https://gitlab.com/snowgoonspub/rust-avr-nightly-builder
cd rust-avr-nightly-builder
./build.sh
```
You will now have a working version of `nightly` Rust for AVR, linked as a
toolchain named `snowgoons`.

# 3. Install the official Microchip AVR toolchain
Currently located [here on microchip.com](https://www.microchip.com/en-us/tools-resources/develop/microchip-studio/gcc-compilers),
you want the *AVR 8-bit Toolchain 3.6.2*.

# 4. Install the other tools you need for your hardware device
At a minimum, you will need `avrdude` to flash your device.

# 5. Download a template application
```

git clone https://gitlab.com/snowgoonspub/avr-oxide-templates
cp -R avr-oxide-templates/atmega4809 my-new-application
```

# 6. Build and run
```
cd my-new-application
cargo run --release
```

And that, as they say, is that!