---
title: "Rust on AVR: Gotchas and Warnings"
date: 2022-01-08
image: "images/post/assembler-debugging.png"
author: "Tim Walls" # use capitalize
description: "Bugs and problems to avoid when developing on AVR with Rust"
categories: ["gettingstarted"]
tags: ["avroxide", "bugs"]
draft: false
---
Developing for the AVR with Rust is not without its trials and tribulations,
and is occasionally hair-pullingly frustrating.

I'll try to keep this page updated as and when I discover issues that are
worth sharing.  And if any of the bugs is ever fixed, I'll also try to
indicate that!

# Rust AVR broken since October 2022
On 23rd October 2022, Rust AVR compilation was broken.  [A fix](https://github.com/rust-lang/rust/pull/106619)
is very slowly working its way through the system, but until that happens
you will need to use `nightly-2022-10-22` to build your AVR applications.

# 32 Bit Maths *was* broken
As per [Rust Issue #82242](https://github.com/rust-lang/rust/issues/82242),
32bit maths was seemingly irretrievably broken with Rust on AVR.  But, only
seemingly, because [Patryk27](https://github.com/Patryk27) heroically discovered
that the problem was Rust generating integer maths builtins with the same
name (but a different signature) from `avr-gcc`'s own library builtins.

Now we know that this can be fixed by adding the following to your `.cargo/config`,
and we can start using 32 bit maths again:

```
[unstable]
build-std-features = ["compiler-builtins-mangled-names"]
```

# Sleep mode and AVR serial comms don't mix...
This is nothing to do with Rust per se, but I just spent ages debugging why
my serial routines weren't working and eventually realised it's because the
USART serial clock doesn't run if the AVR is allowed to go into *SLEEP* mode
(as controlled by the `SLPCTRL` sleep control register.)

*IDLE* mode on the other hand works fine, so, pro-tip: if you have ongoing
serial transmission, ensure that you disable sleep (set the `SMODE` bits
of `SLPCTRL.CTRLA` to `0b00`) for as long as you are transmitting.

