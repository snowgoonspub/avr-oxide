---
title: "Release 0.2.0: We Have Arrived"
date: 2022-01-03
image: "images/post/release-0.2.0.png"
author: "Tim Walls" # use capitalize
description: "Release 0.2.0 - Finally ready for useful applications"
categories: ["AVRoxide"]
tags: ["avroxide", "release"]
draft: false
---
Well, the Christmas and New Year break was a busy one in Snowgoons Towers, and
following hot on the heels of Release 0.1 on Christmas Eve, I'm very happy
to announce 2022 begins with Release 0.2.

This is a bigger release than the new feature list would suggest - I'm going
to go so far as to say that now AVRoxide is actually ready to be used for
building real, practical applications on the ATmega4809 microcontroller.

There are big changes to the [example program](/post/an-example-program/) that
I think illustrate what I mean, and a new [step by step guide](/post/step-by-step/)
to get you up and running quickly.

Enjoy!

# New Features
* The Persist trait can now be derived for basic structs and enums using the
  derive macro provided by `oxide_macros::Persist` 
* We can now set a stdout device using `avr_oxide::stdout::set_stdout()`,
  and can use print! and println! macros, using the `ufmt` crate :-).
* WallClockTimer now supports scheduling events in the future using the
  `after_delay()` method.
* EEPROM access driver, and also a very basic persistence trait implementation.
* Serial port Read methods now implemented.
