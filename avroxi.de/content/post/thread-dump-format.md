---
title: "Thread Dump Format"
date: 2022-02-21
image: "images/post/thread-dumps.jpg"
author: "Tim Walls" # use capitalize
description: "Thread Dump debugging format"
categories: ["reference"]
tags: ["avroxide", "debugging", "threads"]
draft: false
---
The `print_thread_state()` function in the `avr_oxide::util::debug` module
can be used to print a dump of all currently running threads to the
`panicout` serial port.

A typical thread dump looks something like this:
```yaml
T0:
  bgd
  RC: 0
  Wt:
  ST:
    OK
    S: 64
    T: 12033
    F: 44

T1:
  RDY
  RC: 0
  Wt:
  ST:
    OK
    S: 512
    T: 12555
    F: 204

T2:
  Zby
  RC: 123
  Wt:
  ST:
    OK
    S: 1024
    T: 13609
    F: 884

T3:
  RDY
  RC: 0
  Wt:
  ST:
    OK
    S: 256
    T: 14181
    F: 207
```
This shows a total of four threads - two are ready (will be scheduled for
execution,) one is a zombie, and finally the background Idle thread (which
must always be present).  For each thread we can see the state, and also the
current status of the stack allocated to that thread.

# Anatomy of an individual thread
The meaning of each line explained:

| Data           | Meaning                                                         |
|----------------|-----------------------------------------------------------------|
| `T2:`          | Thread ID                                                       |
| `  Zby`        | Thread status, in this case Zombie                              |
| `  RC:123`     | Thread return code, if the thread is completed (otherwise 0)    |
| `  Wt:`        | List of thread IDs waiting to join this thread, if any          |
| `  ST:`        | Stack status follows                                            |
| `    OK`       | Status of the stack: `OK`, or `BAD` if the stack has overflowed |
| `    S:1024`   | Stack size, in bytes                                            |
| `    T:13609`  | Address of the top of the stack in memory                       |
| `    F:884`    | Number of bytes free/unused in this stack                       |

## Thread Status
The following mnemonics are used to represent possible thread status:

| Code&nbsp;&nbsp;&nbsp; | Meaning                                                                                  |
|------------------------|------------------------------------------------------------------------------------------|
| `RDY`                  | Ready to execute                                                                         |
| `bgd`                  | Ready to execute at background priority (only scheduled if no threads are `RDY`)         |
| `Sus`                  | Suspended/cannot be scheduled                                                            |
| `Zby`                  | Zombie; thread has exited, but before it can be freed another thread must `join()` it    |
| `XXX`                  | Thread is dead.  It consumes no resources, and may be replaced by a subsequent `spawn()` |
| `B:M`                  | Blocked on a Mutex                                                                       |
| `B:T`                  | Blocked waiting to `join()` another thread                                               |
| `B:Q`                  | Blocked on a Queue (waiting for an event or I/O)                                         |
| `B:E`                  | Blocked on an EventWait object                                                           |

