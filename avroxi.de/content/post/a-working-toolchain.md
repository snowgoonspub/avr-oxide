---
title: "A Working Rust-on-AVR Toolchain"
date: 2021-12-03
image: "images/post/rust-avr-nightly-builder.png"
author: "Tim Walls" # use capitalize
description: "The Rust AVR toolchain wa broken; this was how to build a less broken one..."
categories: ["gettingstarted"]
tags: ["avroxide", "bugs"]
draft: false
---
# Deprecation Notice
AS of summer 2022, the information below on how to build a custom toolchain
is no longer required - the standard `nightly` toolchain works for AVR again,
thanks to [Patryk27](https://github.com/Patryk27).

I'm leaving the original content below though just in case something happens
in future and we need a reminder how to do it again :-).

# In the dark days...
The Rust on AVR toolchain is currently irretrievably broken because of a 
bug in LLVM.  Hopefully, sooner or later, this will get fixed - but until then
you will need a version of the toolchain that has been patched in order
to build an AVRoxide application.

More [background detail can be found here](https://snowgoons.ro/posts/2021-10-09-the-day-rust-on-avr-stood-still/),
but in the meantime the short version is:  A script which will download
and build a patched version of the toolchain can be found [here](https://gitlab.com/snowgoonspub/rust-avr-nightly-builder);
this script will install the toolchain locally with the name `snowgoons`.  Once
built, you can then reference it in your `rust-toolchain.toml` file.

To use the script and build the toolchain:

```
git clone https://gitlab.com/snowgoonspub/rust-avr-nightly-builder
cd rust-avr-nightly-builder
./build.sh
```

Then update your `rust-toolchain.toml` as follows:
```
[toolchain]
channel = "snowgoons"
components = ["rust-src"]
```