---
title: "AVRoxide"
date: 2021-08-28
image: "images/post/intro-slim.jpg"
author: "Tim Walls" # use capitalize
description: "The Rusty runtime for ATmega AVR microcontrollers"
categories: ["AVRoxide"]
tags: ["avroxide"]
draft: false
weight: -100
---
Welcome!  You have found yourself on the homepage of [AVRoxide] - a [Rust]
runtime for [ATmega] [AVR] microcontrollers.

# Why?
AVRoxide was borne out of [my] frustration with attempting to develop for an
[Arduino Nano Every] microcontroller board using the Rust programming
language.

The Arduino Nano Every uses a different version of AVR microcontroller to
the other Arduino family of boards - the [ATmega4809].  While there are
HAL[^1] crates available for various Arduino boards, none of them works with
this microcontroller, and that's a shame - it's actually a super nice chip
and a good upgrade on the ones used in the 'traditional' Arduino boards.  So,
support for it in Rust would obviously be nice to have.

## How we got here
Naturally, the first thing I tried to do was to [adapt](https://snowgoons.ro/posts/2020-11-11-compiling-rust-for-arduino-nano-every-part-one/) 
some of [the existing](https://snowgoons.ro/posts/2021-08-11-rust-on-arduino-nano-every-part-two/)
HALs to work with the Nano Every.  But this turned out to be a little
trickier than I anticipated, and in any event I discovered there were some
things that I didn't like about those HALs.  This is in no way a reflection
on the quality of thoes HALs - much more a reflection on my Rust abilities -
but I found I wanted something a little simpler and easier to understand.

And so, the idea of putting together my own HAL come to mind.  But, I also
wanted to move a little higher up the pipeline - not just to abstract away
the hardware, but also to put in place some standard patterns for things
like a standard event loop that would hide some of the details of things like
interrupt handling from the application developer.

Thus, AVRoxide is born!  The Rusty runtime for AVR microcontrollers.

# Colophon
## This Site
The intention of this site is to provide a home for simple getting started
type documentation - the "user guide" for developers wanting to write
applications for AVR devices using the AVRoxide runtime, if you like.  It
is not intended to replace the detailed API level [Rustdocs].

## Licensing
AVRoxide itself is open source software, distributed under the [BSD 3 Clause]
license.  You can download the source code from the [Gitlab Repository].

## The Author
Tim Walls has been churning out code of highly variable quality for the
better part of 40 years.  From Commodore64 BASIC, via Intel 8051 assembly code,
and of course C, C++, Java and a few other things along the way, he has more
recently settled on Rust as his language of choice.  You can read more on
his blog, [snowgoons](https://snowgoons.ro/).

[AVRoxide]: https://avroxi.de/
[Rust]: https://www.rust-lang.org/what/embedded
[ATmega]: https://www.microchip.com/
[AVR]: https://www.microchip.com/en-us/products/microcontrollers-and-microprocessors/8-bit-mcus/avr-mcus
[my]: https://snowgoons.ro/
[ATmega4809]: https://www.microchip.com/en-us/products/microcontrollers-and-microprocessors/8-bit-mcus/avr-mcus/atmega4809
[Arduino Nano Every]: https://docs.arduino.cc/hardware/nano-every
[Rustdocs]: https://pages.avroxi.de/
[BSD 3 Clause]: https://opensource.org/licenses/BSD-3-Clause
[Gitlab Repository]: https://gitlab.com/snowgoonspub/avr-oxide


[^1]: Hardware Abstraction Layer
