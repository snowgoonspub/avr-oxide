#!/bin/bash

# If we have a CI commit ref, use it - because we will set up image
# retention policies in Gitlab based on those
BRANCH=${CI_COMMIT_REF_NAME:-$(git rev-parse --short HEAD)}
export IMAGETAG=${BRANCH/\//-}

echo "Deploying with tag: ${IMAGETAG}"

export THEME=persian-hugo
export REGISTRY=registry.svc0.prod.baneasa.snowgoons.ro/snowgoonspub/avr-oxide

echo "Fetching template..."
git submodule init
git submodule update

echo "Building site..."
hugo --themesDir . --theme ${THEME}

echo "Building Docker image..."
docker build -t ${REGISTRY}/website:${IMAGETAG} .
docker push ${REGISTRY}/website:${IMAGETAG}

echo "Deploying to Kubernetes..."
envsubst < deploy.yaml | kubectl apply -f -