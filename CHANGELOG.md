# Release-0.4.2
* Minor changes to let us build with current toolchains (currently pinned
  to `nightly-2024-05-01`)
# Release-0.4.1
* Minor build and documentation changes required to note that we now need to
  pin builds to `nightly-2022-10-22` until Rust-AVR code generation is fixed.
# Release-0.4.0
* Storage drivers for composite chips (devices with multiple chips that
  present as multiple I2C addresses)
* Buffered storage driver overlay
* Simple filesystem - SNaFus - that works with our storage drivers.
* Graphics driver and rendering syssystem in new `avrox-display` crate.
* Support for DisplayVisions EA-W096016 OLED display.
* Support for Adafruit 128x128 greyscale OLED display.
* Trivial Bitmap File Format (TBFF) support - see [AVRoxide Utils](https://gitlab.com/snowgoonspub/avrox-utils) for a simple converter app.
* No longer needs a custom built toolchain, `nightly` will do
# Release-0.3.1
* TWI (Two-Wire Interface) master driver (both HAL and userland)
* Serial EEPROM driver in the `avrox-storage` crate for the Rohm `BR24T1M-3AM`
# Release-0.3.0
* Well behaved blocking reads on serial ports, and options for buffered
  read-to-EOL and read-to-EOT
* Main thread stack size now configurable using an attribute on the 
 `avr_oxide::main` macro.
* Better stack-overflow detection to detect stack busts where the compiler
  skips stack elements by modifying the SP directly, and also a new function
  in the `avr_oxide::util::debug` module to print out details of all current
  threads to the configured panicout serial port (including stack usage)
  to help developers right-size their thread stack sizes.
* Reduced size of code segment through refactoring Port implementation,
* And reduced stack use in interrupts/system calls.
* Now includes basic Mutex synchronisation primitive, and the various I/O
  routines have been updated to use thread yield/waits instead of spinlocking.
* Change the way we do Arduino-compatible pin naming to avoid creating huge
  objects on the stack.
* Pre-emptive multithreading
* Dynamic sizing of the heap to use all available memory, losing the 
  `alloc_large` style feature flags 
* Reduced memory use for ports/pins (in particular to accomodate the '328p)
* Events scheduled with `after_delay()` now return a handle that can be
  used to cancel them before they run using `cancel_delay()`
* MasterClock can now schedule delay events in the same way as WallClock
# Release-0.2.2
* Configurable power-saving with `power_low` and `power_med` feature flags
* Arduino Uno/ATmega328p support started - basic pins, serial support.
* Fix huge bug with TX Complete interrupt eating all the CPU time.
* Workarounds for LLVM interrupt code generation bugs.
# Release-0.2.1
* Persist macro works for structs with anonymous fields.
* New serial port flags:  `echo`
# Release-0.2.0
* The Persist trait can now be derived for basic structs and enums using the
  derive macro provided by `oxide_macros::Persist` 
* We can now set a stdout device using `avr_oxide::stdout::set_stdout()`,
  and can use print! and println! macros, using the `ufmt` crate :-).
* WallClockTimer now supports scheduling events in the future using the
  `after_delay()` method.
* EEPROM access driver, and also a very basic persistence trait implementation.
* Serial port Read methods now implemented.
# Version 0.1.0
* A stable event handling model I think I'm happy with! :-D
* Bugfixes to the allocator, which is now reliable :-).
* Serial port HAL device now flushes more intelligently.
* Serial port abstract device can now be configured to do things like CRLF
  translation and automatic flush on end-of-line.
* Much nicer event handling model with on_event() callback interface on the
  various device drivers.  (Requires heap memory allocation to work though;
  the old mechanism still works for no-heap use.)
* Implemnentation of a driver for the AVR RTC device.
* We can debounce pin inputs now by wrapping the Pin with a Debouncer
# Version 0.0.6
* Use custom build toolchain to work around LLVM bugs
* `[avr_oxide::main]` macro for declaring application entrypoint.
* Added source identities to the Oxide events.
* Codesize improvements, and ability to enable individual drivers with feature
  flags.
* Dynamic memory allocater (enabled with `alloc_` feature flags)
# Version 0.0.5
* Very basic mocks of devices adequate to at least allow client applications
  to run their own unit-tests using `cargo test`
* Removed all dependence on the `avr-device` HAL crates
* Serial (USART) support, with interrupt driven transmit/receive and buffering
* Watchdog timer support
* SerialPort abstraction that works with Oxide events and implements standard
  write methods (core::fmt::Write, io::Write)
* Optional global `stdout!()` macro for accessing one of the serial ports,
  enabled using the `stdout_usart0` ... `stdout_usart3` features.
* Panic handler can now print out something useful (file:line:col location)
  to the serial port, if you enabled the stdout feature.
# Version 0.0.4
* MasterClock timer tick rate now parameterised
* Support for all four Type B timers in the ATmega4809
* Main event queue supports coalescing similar events to avoid queue exhaustion
  when a series of similar events (e.g. clock ticks) arrives faster than can
  be processed.
  * Implemented for MasterClock timer events
# Version 0.0.3
* Interrupt handling for GPIO attached buttons
* Working event handling for timer and button interrupts, and a basic supervisor
# Version 0.0.2
* 'Working' CI/CD (for certain values of)
* Documentation build & avroxi.de site build
# Version 0.0.1
* Basic project skeleton in place