/* nc.rs
 *
 * Developed by Tim Walls <tim.walls@snowgoons.com>
 * Copyright (c) All Rights Reserved, Tim Walls
 */
//! A dummy 'not connected' Pin implementation


// Imports ===================================================================
use avr_oxide::alloc::boxed::Box;
use avr_oxide::hal::generic::port::{InterruptMode, Pin, PinIsrCallback, PinMode};
use avr_oxide::util::OwnOrBorrow;

// Declarations ==============================================================
pub struct NC {
  _dummy: u8
}

// Code ======================================================================

// Tests =====================================================================
impl Into<OwnOrBorrow<'static, dyn Pin + 'static>> for NC {
  fn into(self) -> OwnOrBorrow<'static, dyn Pin> {
    OwnOrBorrow::Own(Box::new(self))
  }
}

impl NC {
  pub fn new() -> Self {
    NC {
      _dummy: 0x00u8
    }
  }
}

impl Pin for NC {
  fn set_mode(&self, _mode: PinMode) {
  }

  fn toggle(&self) {
  }

  fn set_high(&self) {
  }

  fn set_low(&self) {
  }

  fn set(&self, _high: bool) {
  }

  fn get(&self) -> bool {
    false
  }

  fn set_interrupt_mode(&self, _mode: InterruptMode) {
  }

  fn listen(&'static self, _handler: PinIsrCallback) {
  }
}