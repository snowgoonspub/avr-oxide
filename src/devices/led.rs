/* led.rs
 *
 * Developed by Tim Walls <tim.walls@snowgoons.com>
 * Copyright (c) All Rights Reserved, Tim Walls
 */
//! Simple abstraction of an LED attached to a GPIO pin.
//!
//! In all honestly, it doesn't abstract much ;).  But it does allow a
//! consistent method for using/creating by implementing the [`avr_oxide::devices::UsesPin`] trait,
//! and being compatible with the Handle trait.
//!
//! # Usage
//! ```no_run
//! # #![no_std]
//! # #![no_main]
//! #
//! # use avr_oxide::devices::UsesPin;
//! # use avr_oxide::devices::OxideLed;
//! # use avr_oxide::boards::board;
//!
//! # #[avr_oxide::main(chip="atmega4809")]
//! # pub fn main() {
//! #   let supervisor = avr_oxide::oxide::instance();
//!
//!   let green_led = OxideLed::with_pin(board::pin_d(7));
//!
//!   green_led.toggle();
//! #
//! #  supervisor.run();
//! # }
//! ```


// Imports ===================================================================
use avr_oxide::hal::generic::port::{Pin, PinMode, InterruptMode};
use avr_oxide::util::OwnOrBorrow;
use avr_oxide::devices::UsesPin;


// Declarations ==============================================================
pub struct Led {
  pin: OwnOrBorrow<'static,dyn Pin>
}


// Code ======================================================================
impl UsesPin for Led {
  fn using<OP: Into<OwnOrBorrow<'static, dyn Pin>>>(pin: OP) -> Self {
    let pin : OwnOrBorrow<dyn Pin> = pin.into();

    pin.set_mode(PinMode::Output);
    pin.set_interrupt_mode(InterruptMode::Disabled);
    Led { pin }
  }
}

impl Led {
  pub fn set_on(&self) {
    self.pin.set_high();
  }

  pub fn set_off(&self) {
    self.pin.set_low();
  }

  pub fn set(&self, on: bool) {
    match on {
      true => self.set_on(),
      false => self.set_off()
    }
  }

  pub fn toggle(&self) {
    self.pin.toggle()
  }
}

unsafe impl Send for Led {}
unsafe impl Sync for Led {}