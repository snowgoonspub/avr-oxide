/* mutstatic.rs
 *
 * Developed by Tim Walls <tim.walls@snowgoons.com>
 * Copyright (c) All Rights Reserved, Tim Walls
 */
//! Mutable static singleton helper

#[doc(hidden)]
#[macro_export]
macro_rules! mut_singleton {
  ($type:ty, $name:ident, $accessor:ident, $accessor_isolated:ident, $constructor:expr) => {
    static mut $name : Option<$type> = None;

    /**
     * Return a reference to the global singleton instance of $type.  The
     * caller should pass an isolation token obtained by
     * [`avr_oxide::concurrency::interrupt::isolated()`]
     */
    #[allow(dead_code)]
    #[inline(never)]
    pub fn $accessor_isolated(_isotoken: avr_oxide::concurrency::Isolated) -> &'static mut $type {
      unsafe {
        if $name.is_none() {
          core::ptr::replace(&mut $name, Some($constructor));
        }

        match &mut $name {
          None => avr_oxide::oserror::halt(avr_oxide::oserror::OsError::InternalError),
          Some(instance) => instance
        }
      }
    }
    /**
     * Return a reference to a global singleton instance of $type.
     * Interrupts will be locked for the duration of the call to ensure
     * consistency; if calling from a context wherein interrupts are
     * already disabled, calling $accessor_isolated() will be more
     * efficient.
     */
    #[allow(dead_code)]
    pub fn $accessor() -> &'static mut $type {
      avr_oxide::concurrency::interrupt::isolated(|isotoken|{
        $accessor_isolated(isotoken)
      })
    }
  };
}

#[doc(hidden)]
#[macro_export]
macro_rules! mut_singleton_explicit_init {
  ($type:ty, $name:ident, $initialiser:ident, $accessor:ident, $accessor_isolated:ident, $constructor:expr) => {
    static mut $name : core::mem::MaybeUninit<$type> = core::mem::MaybeUninit::uninit();

    /**
     * Inititalise the global singleton of $type.
     */
    pub unsafe fn $initialiser() {
      core::ptr::write(
        $name.as_mut_ptr(),
        $constructor
      );
    }

    /**
     * Return a reference to the global singleton instance of $type.  The
     * caller should pass an isolation token obtained by
     * [`avr_oxide::concurrency::interrupt::isolated()`]
     */
    #[allow(dead_code)]
    pub fn $accessor_isolated(_isotoken: avr_oxide::concurrency::Isolated) -> &'static mut $type {
      unsafe {
        $name.assume_init_mut()
      }
    }

    /**
     * Return a reference to a global singleton instance of $type.
     * Interrupts will be locked for the duration of the call to ensure
     * consistency; if calling from a context wherein interrupts are
     * already disabled, calling $accessor_isolated() will be more
     * efficient.
     */
    #[allow(dead_code)]
    pub fn $accessor() -> &'static mut $type {
      avr_oxide::concurrency::interrupt::isolated(|isotoken|{
        unsafe {
          $name.assume_init_mut()
        }
      })
    }
  };
}

