/* mod.rs
 *
 * Developed by Tim Walls <tim.walls@snowgoons.com>
 * Copyright (c) All Rights Reserved, Tim Walls
 */
//! Abstraction of the hardware devices attached to an AVR microcontroller

// Imports ===================================================================
pub mod generic;

#[cfg(feature="atmega4809")]
pub mod atmega4809;

#[cfg(feature="atmega328p")]
pub mod atmega328p;

#[cfg(feature="panic_handler")]
#[panic_handler]
#[cfg(target_arch="avr")]
fn panic(info: &core::panic::PanicInfo) -> ! {
  #![allow(deprecated)] // until asm! is implemented for AVR
  unsafe {
    avr_oxide::concurrency::interrupt::disable_interrupts();

    #[cfg(feature="panicout")]
      {
        use avr_oxide::util::debug;
        // This doesn't look too pretty, but we can't use any of the standard
        // formatting routines because they require an allocator, which even
        // if we have one, is probably broken right now.
        debug::print("\n\nPanic:\n");
        if let Some(location) = info.location() {
          debug::print("   --> ");
          debug::print(&location.file());
          debug::print(":");
          debug::print_u16(location.line() as u16);
          debug::print(":");
          debug::print_u16(location.column() as u16);
          debug::print("\n");
        }
      }

    avr_oxide::oserror::halt(avr_oxide::oserror::OsError::Panic);
  }
}
