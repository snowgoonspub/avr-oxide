/* callback.rs
 *
 * Developed by Tim Walls <tim.walls@snowgoons.com>
 * Copyright (c) All Rights Reserved, Tim Walls
 */
//! Reasonably generic way of dealing with callbacks from interrupt handlers.
//!

// Imports ===================================================================
use core::any::Any;

// Declarations ==============================================================
/**
 * Represents a callback function from an interrupt service routine.
 */
#[derive(Copy,Clone,Debug)]
pub enum IsrCallback<F,R>
{
  /// No Operation - dummy callback that evaluates to a fixed constant return
  /// value.
  Nop(R),
  /// A function/closure callback.  This will be called when the interrupt
  /// service routine needs it; the function signature is context dependent,
  /// however the last parameter will always be an `Option<&'static dyn Any>`
  /// user-data field, which will be given a `None` value.
  /// The first parameter must be an isolation token
  /// ([`avr_oxide::concurrency::Isolated`])
  Function(F),
  /// A function/closure callback.  This will be called when the interrupt
  /// service routine needs it; the function signature is context dependent,
  /// however the last parameter will always be an `Option<&'static dyn Any>`
  /// user-data field, into which the given data will be passed as Some()
  /// value.
  /// The first parameter must be an isolation token
  /// ([`avr_oxide::concurrency::Isolated`])
  WithData(F, *const dyn Any)
}

// Code ======================================================================
#[doc(hidden)]
#[macro_export]
macro_rules! isr_cb_invoke {
  ($isotoken:expr, $cb:expr, $($params:expr),*) => {
    {
      let cb : avr_oxide::hal::generic::callback::IsrCallback<_,_> = $cb;

      match cb {
        avr_oxide::hal::generic::callback::IsrCallback::Nop(r) => r,
        avr_oxide::hal::generic::callback::IsrCallback::Function(f) => (f)($isotoken, $($params),*, Option::None),
        avr_oxide::hal::generic::callback::IsrCallback::WithData(f,d) => (f)($isotoken, $($params),*, Option::Some(d))
      }
    }
  }
}

impl<F,R> IsrCallback<F,R> {
  /**
   * Return true iff this callback is a no-op function.  We can use this to
   * drive optimisations like disabling the interrupt routine entirely.
   */
  pub fn is_nop(&self) -> bool {
    match self {
      IsrCallback::Nop(_) => true,
      IsrCallback::Function(_) => false,
      IsrCallback::WithData(_, _) => false
    }
  }
}


// Tests =====================================================================
