/* mod.rs
 *
 * Developed by Tim Walls <tim.walls@snowgoons.com>
 * Copyright (c) All Rights Reserved, Tim Walls
 */
//! Generic traits for the various devices that can be attached to our AVR
//! device
use core::marker::PhantomData;

pub mod datatypes;
pub mod callback;
pub mod timer;
pub mod port;
pub mod watchdog;
pub mod cpu;
pub mod serial;
pub mod eeprom;
pub mod twi;

#[lang = "eh_personality"]
#[no_mangle]
#[cfg(target_arch="avr")]
#[allow(clippy::missing_safety_doc)]
pub unsafe extern "C" fn rust_eh_personality() {
}

pub(crate) struct MutDeviceRef<T> {
  address: usize,
  _phantom: PhantomData<T>
}

impl<T> MutDeviceRef<T> {
  pub fn new(devaddr: u16) -> Self {
    MutDeviceRef {
      address: devaddr as usize,
      _phantom: PhantomData::default()
    }
  }

  pub fn borrow_mut(&self, _isotoken: avr_oxide::concurrency::interrupt::token::Isolated) -> &mut T {
    unsafe {
      core::mem::transmute(self.address as *mut T)
    }
  }
}

/**
 * Trait implemented to allow us to set portmux control bits
 */
pub trait MuxControl {
  /**
   * Set the relevant hardware controls to route this appropriately.
   */
  fn route(&self);
}