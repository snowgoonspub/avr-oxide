;;; ATMega Thread context save/restore
;;;
;;; Tim Walls <tim.walls@snowgoons.com>
;;; Copyright (c) 2022, ALl Rights Reserved
;;;
;;; ATmega328p version
;;;
#define CURRENT_CONTEXT_LO 0x2A
#define CURRENT_CONTEXT_HI 0x2B
#define SREG               0x3F
#define SPH                0x3E
#define SPL                0x3D

#define CONTEXT_FLAGS      0x1E
#define CONTEXT_RESTORE_FLAG      0x1E,0
#define CONTEXT_RETI_FLAG         0x1E,1
#define CONTEXT_ENABLEINTS_FLAG   0x1E,2

#define KERNEL_STACK_SIZE 128

#include "../generic/context.S"

