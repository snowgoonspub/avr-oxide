/* serial.rs
 *
 * Developed by Tim Walls <tim.walls@snowgoons.com>
 * Copyright (c) All Rights Reserved, Tim Walls
 */
//! ATmega4809-specific serial device declaration/implementation

// Imports ===================================================================


// Declarations ==============================================================


#[cfg(feature="panicout_usart0")]
pub mod panicout {
  #[doc(hidden)]
  #[macro_export]
  macro_rules! panic_stdout {
    () => {
      avr_oxide::hal::atmega328p::serial::usart0::instance()
    }
  }
}

// Code ======================================================================
#[cfg(feature="usart0")]
pub mod usart0 {
  use avr_oxide::{atmel_simple_usart_tpl};

  atmel_simple_usart_tpl!(super::super::ADDR_USART,
    avr_oxide::hal::generic::serial::SerialPortIdentity::Usart0,
    avr_oxide::hal::generic::serial::base::simple::AtmelUsart<{avr_oxide::deviceconsts::oxide::SERIAL_BUFFER}>,
    "usart_rcx", "usart_tcx", "usart_dre");
}
