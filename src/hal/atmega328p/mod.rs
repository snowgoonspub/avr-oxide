/* mod.rs
 *
 * Developed by Tim Walls <tim.walls@snowgoons.com>
 * Copyright (c) All Rights Reserved, Tim Walls
 */
//! ATmega328P implementations of the generic device traits
#![allow(unused)]

use core::arch::asm;
use avr_oxide::hal::generic::serial::SerialNoInterruptTx;

pub mod cpu;
pub mod port;
pub mod serial;


const ADDR_CPU:    u16 = 0x005D;

const ADDR_PORTB:  u16 = 0x0023;
const ADDR_PORTC:  u16 = 0x0026;
const ADDR_PORTD:  u16 = 0x0029;

const ADDR_USART:  u16 = 0x00C0;
const ADDR_CLKPR:  u16 = 0x0061;

const ADDR_PCICR:  u16 = 0x0068;
const ADDR_PCIFR:  u16 = 0x003B;
const ADDR_PCMSK0: u16 = 0x006B;
const ADDR_PCMSK1: u16 = 0x006C;
const ADDR_PCMSK2: u16 = 0x006D;

/**
 * Inititalise all the configured hardware devices/drivers attached to this
 * device.
 *
 * # SAFETY
 * This must be called not only while interrupts are disabled, but
 * *before interrupts are enabled for the first time*, or you can expect
 * bad stuff to happen.
 */
pub(crate) unsafe fn initialise() {
  // Serial ports
  #[cfg(feature="usart0")]
    serial::usart0::initialise();
}