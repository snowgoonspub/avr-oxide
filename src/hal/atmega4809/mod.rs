/* mod.rs
 *
 * Developed by Tim Walls <tim.walls@snowgoons.com>
 * Copyright (c) All Rights Reserved, Tim Walls
 */
//! ATmega4809 implementations of the generic device traits
#![allow(unused)]

use core::arch::asm;
use avr_oxide::hal::generic::serial::SerialNoInterruptTx;

pub mod cpu;
pub mod timer;
pub mod port;
pub mod watchdog;
pub mod serial;
pub mod nvm;
pub mod twi;


const ADDR_CPU:     u16 = 0x0030;
const ADDR_CPUINT:  u16 = 0x0110;
const ADDR_SLPCTRL: u16 = 0x0050;
const ADDR_CLKCTRL: u16 = 0x0060;
const ADDR_PORTA:   u16 = 0x0400;
const ADDR_PORTB:   u16 = 0x0420;
const ADDR_PORTC:   u16 = 0x0440;
const ADDR_PORTD:   u16 = 0x0460;
const ADDR_PORTE:   u16 = 0x0480;
const ADDR_PORTF:   u16 = 0x04A0;
const ADDR_PORTMUX: u16 = 0x05E0;
const ADDR_USART0:  u16 = 0x0800;
const ADDR_USART1:  u16 = 0x0820;
const ADDR_USART2:  u16 = 0x0840;
const ADDR_USART3:  u16 = 0x0860;
const ADDR_TWI0:    u16 = 0x08A0;
const ADDR_TCB0:    u16 = 0x0A80;
const ADDR_TCB1:    u16 = 0x0A90;
const ADDR_TCB2:    u16 = 0x0AA0;
const ADDR_TCB3:    u16 = 0x0AB0;
const ADDR_WDT:     u16 = 0x0100;
const ADDR_RTC:     u16 = 0x0140;
const ADDR_NVMCTRL: u16 = 0x1000;
const ADDR_USERROW_PBUF: u16 = 0x1300;
const ADDR_EEPROM_PBUF:  u16 = 0x1400;
const EEPROM_PAGESIZE : usize = 64;
const EEPROM_PAGES    : usize = 4;
const USERROW_PAGESIZE: usize = 64;
const USERROW_PAGES   : usize = 1;

/**
 * Inititalise all the configured hardware devices/drivers attached to this
 * device.
 *
 * # SAFETY
 * This must be called not only while interrupts are disabled, but
 * *before interrupts are enabled for the first time*, or you can expect
 * bad stuff to happen.
 */
pub(crate) unsafe fn initialise() {
  // Timers
  #[cfg(feature="tcb0")]
    timer::tcb0::initialise();
  #[cfg(feature="tcb1")]
    timer::tcb1::initialise();
  #[cfg(feature="tcb2")]
    timer::tcb2::initialise();
  #[cfg(feature="tcb3")]
    timer::tcb3::initialise();
  #[cfg(feature="rtc")]
    timer::rtc::initialise();

  // Serial ports
  #[cfg(feature="usart0")]
    serial::usart0::initialise();
  #[cfg(feature="usart1")]
    serial::usart1::initialise();
  #[cfg(feature="usart2")]
    serial::usart2::initialise();
  #[cfg(feature="usart3")]
    serial::usart3::initialise();

  #[cfg(feature="twi0")]
    twi::twi0::initialise();

  // Non-Volatile storage
  nvm::eeprom::initialise();
  nvm::userrow::initialise();
}