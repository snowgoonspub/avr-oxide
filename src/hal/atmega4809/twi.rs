/* twi.rs
 *
 * Developed by Tim Walls <tim.walls@snowgoons.com>
 * Copyright (c) All Rights Reserved, Tim Walls
 */
//! ATmega4809  Two-Wire Interface bus devices

// Imports ===================================================================

// Declarations ==============================================================

// Code ======================================================================
#[cfg(feature="twi0")]
pub mod twi0 {
  use avr_oxide::atmel_twi_tpl;
  use avr_oxide::hal::atmega4809::cpu::PortmuxControl;
  use avr_oxide::hal::generic::MuxControl;
  use avr_oxide::util::datatypes::{BitFieldAccess, BitRange};
  use avr_oxide::hal::generic::twi;

  pub enum TwiPins {
    MasterASlaveC,
    MasterASlaveF,
    MasterCSlaveF
  }

  impl MuxControl for TwiPins {
    #[cfg(target_arch="avr")]
    fn route(&self) {
      unsafe {
        let pmux : &mut PortmuxControl = core::mem::transmute(super::super::ADDR_PORTMUX);

        const TWI0: BitRange = BitRange::range_c(4,5);

        pmux.twispiroutea.set_to(TWI0, match self {
          TwiPins::MasterASlaveC => 0x00,
          TwiPins::MasterASlaveF => 0x01,
          TwiPins::MasterCSlaveF => 0x02
        });
      }
    }

    #[cfg(not(target_arch="avr"))]
    fn route(&self) {
      println!("*** TWI0:PORTMUX: Routed portmux");
    }
  }

  atmel_twi_tpl!(super::super::ADDR_TWI0,
    twi::TwiIdentity::Twi0master, twi::TwiIdentity::Twi1slave,
    twi::base::zeroseries::AtmelTwi<TwiPins>,
    "twi0_twim", "twi0_twis");
}


// Tests =====================================================================
