/* timer.rs
 *
 * Developed by Tim Walls <tim.walls@snowgoons.com>
 * Copyright (c) All Rights Reserved, Tim Walls
 */
//! ATmega4809 specific implementation of the TimerControl traits for the
//! device TCBs.

// Imports ===================================================================


// Declarations ==============================================================


// Code ======================================================================
#[cfg(feature="tcb0")]
pub mod tcb0 {
  use avr_oxide::atmel_tcb;

  atmel_tcb!(super::super::ADDR_TCB0, avr_oxide::hal::generic::timer::TimerIdentity::Tcb0, "tcb0_int");
}
#[cfg(feature="tcb1")]
pub mod tcb1 {
  use avr_oxide::atmel_tcb;
  atmel_tcb!(super::super::ADDR_TCB1, avr_oxide::hal::generic::timer::TimerIdentity::Tcb1, "tcb1_int");
}
#[cfg(feature="tcb2")]
pub mod tcb2 {
  use avr_oxide::atmel_tcb;

  atmel_tcb!(super::super::ADDR_TCB2, avr_oxide::hal::generic::timer::TimerIdentity::Tcb2, "tcb2_int");
}
#[cfg(feature="tcb3")]
pub mod tcb3 {
  use avr_oxide::atmel_tcb;

  atmel_tcb!(super::super::ADDR_TCB3, avr_oxide::hal::generic::timer::TimerIdentity::Tcb3, "tcb3_int");
}
#[cfg(feature="rtc")]
pub mod rtc {
  use avr_oxide::atmel_rtc;

  atmel_rtc!(super::super::ADDR_RTC, avr_oxide::hal::generic::timer::TimerIdentity::Rtc, "rtc_pit");
}


// Tests =====================================================================
