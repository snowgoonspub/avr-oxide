/* watchdog.rs
 *
 * Developed by Tim Walls <tim.walls@snowgoons.com>
 * Copyright (c) All Rights Reserved, Tim Walls
 */
//! ATmega4809-specific implementation of the AVR watchdog timer

// Imports ===================================================================
use avr_oxide::{atmel_watchdog_tpl};

// Declarations ==============================================================

// Code ======================================================================
atmel_watchdog_tpl!(super::ADDR_WDT, avr_oxide::hal::atmega4809::cpu::instance());