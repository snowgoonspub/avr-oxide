/* eeprom.rs
 *
 * Developed by Tim Walls <tim.walls@snowgoons.com>
 * Copyright (c) All Rights Reserved, Tim Walls
 */
//! Concrete instantiations of the EEPROM spaces available on the ATmega4809.

// Imports ===================================================================


// Declarations ==============================================================
pub mod eeprom {
  use avr_oxide::atmel_nvmctrl_eeprom_tpl;

  atmel_nvmctrl_eeprom_tpl!(
    super::super::ADDR_NVMCTRL,
    super::super::ADDR_EEPROM_PBUF,
    super::super::EEPROM_PAGESIZE,
    super::super::EEPROM_PAGES,
    avr_oxide::hal::atmega4809::cpu::instance()
  );
}

pub mod userrow {
  use avr_oxide::atmel_nvmctrl_eeprom_tpl;

  atmel_nvmctrl_eeprom_tpl!(
    super::super::ADDR_NVMCTRL,
    super::super::ADDR_USERROW_PBUF,
    super::super::USERROW_PAGESIZE,
    super::super::USERROW_PAGES,
    avr_oxide::hal::atmega4809::cpu::instance()
  );
}
