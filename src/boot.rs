/* boot.rs
 *
 * Developed by Tim Walls <tim.walls@snowgoons.com>
 * Copyright (c) All Rights Reserved, Tim Walls
 */
//! Boot process for the Oxide operating system.  Initialises memory and
//! interrupt vectors, loads our hardware table, and then calls the
//! oxide_main() method provided by whoever is using our crate.

// Imports ===================================================================
use avr_oxide::concurrency;
use avr_oxide::concurrency::thread;
use avr_oxide::hal::generic::cpu::{ClockControl, SleepControl};
use avr_oxide::alloc;
use avr_oxide::deviceconsts::clock;

// Declarations ==============================================================
extern {
  fn __oxide_main() -> !;

  static __OXIDE_MAIN_THREAD_STACK_SIZE : usize;
}

// Code ======================================================================
#[no_mangle]
fn _oxide_boot() {
  unsafe {
    // This needs to be initialised absolutely as the first thing.
    alloc::initialise();

    // Inititalise the kernel threading/concurrency system
    concurrency::scheduler::initialise();

    // Set up any hardware-specific configuration
    avr_oxide::hardware::initialise();
    avr_oxide::hardware::cpu::sleepctrl().reset();
    avr_oxide::hardware::cpu::clock().clk_per_prescaler(clock::MASTER_CLOCK_PRESCALER);



    #[cfg(feature="boardcompat")]
    avr_oxide::boards::board::initialise();

    // Initialise the Oxide supervisor
    avr_oxide::oxide::initialise();

    // Spawn the main thread
    let _jh = thread::Builder::new().stack_size(__OXIDE_MAIN_THREAD_STACK_SIZE).spawn(||{
      __oxide_main();
    });

    // Start scheduling
    concurrency::scheduler::restore_first_thread();
  }
}
