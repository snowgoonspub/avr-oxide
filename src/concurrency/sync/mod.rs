/* sync.rs
 *
 * Developed by Tim Walls <tim.walls@snowgoons.com>
 * Copyright (c) All Rights Reserved, Tim Walls
 */
//! Synchronisation primitives for AVRoxide.

// Imports ===================================================================
mod eventwait;
mod mutex;
mod arc;

pub use self::mutex::{Mutex, MutexGuard};
pub use self::eventwait::{EventWait};
pub use self::arc::{Arc};

// Declarations ==============================================================


// Code ======================================================================

// Tests =====================================================================
