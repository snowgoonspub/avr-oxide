/* mod.rs
 *
 * Developed by Tim Walls <tim.walls@snowgoons.com>
 * Copyright (c) All Rights Reserved, Tim Walls
 */
//! Concurrency/threading for AVToxide

// Imports ===================================================================
pub mod scheduler;
pub mod thread;
pub mod interrupt;
pub mod sync;
pub mod util;
pub(crate) mod stack;

use avr_oxide::OxideResult;

pub use interrupt::token::Isolated as Isolated;

// Declarations ==============================================================
/// A type alias for the result of a nonblocking locking method.
pub type TryLockResult<Guard> = OxideResult<Guard, TryLockError>;

/// An enumeration of possible errors associated with a [`TryLockResult`] which
/// can occur while trying to acquire a lock.
pub enum TryLockError {
  /// The lock could not be acquired at this time because the operation would
  /// otherwise block.
  WouldBlock,
}


// Code ======================================================================


// Tests =====================================================================
