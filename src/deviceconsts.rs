/* deviceconsts.rs
 *
 * Developed by Tim Walls <tim.walls@snowgoons.com>
 * Copyright (c) All Rights Reserved, Tim Walls
 */
//! Device-specific constants.  These are determined by the feature flags
//! configued in your `Cargo.toml`.
//!
//! # Features
//! The following feature flags affect these constants
//!
//! | Type | Values |
//! | ---- | ------ |
//! | CPU identification | `atmega4809`, `atmega328p` |
//! | Clockspeed | `20MHz`, `16MHz` |
//! | Power saving mode | None, `power_med`, `power_low` |
//!

// Imports ===================================================================
// Declarations ==============================================================
#[cfg(not(any(feature="20MHz", feature="16MHz")))]
compile_error!("A clockspeed feature (\"16MHz\", \"20MHz\") must be enabled for this crate");

#[cfg(not(any(feature="atmega4809", feature="atmega328p")))]
compile_error!("A processor feature (\"atmega4809\", \"atmega328p\") must be enabled for this crate");

#[cfg(feature = "atmega4809")]
pub mod memory {
  pub const FLASH_SIZE  : usize  = 49_152;
  pub const SRAM_SIZE   : usize  =  6_144;
  pub const SRAM_END    : usize  = 0x3FFF;
  pub const EEPROM_SIZE : usize  =    256;
  pub const USER_SIZE   : usize  =     64;

  pub mod alloc {
    pub const MIN_HEAPSIZE : usize = 2560;
    pub const SMAX         : usize = 128;
  }

  pub mod eeprom {
    pub const EEPROM_SIZE      : usize = 256;
    pub const USERROW_SIZE     : usize = 64;
  }
}

#[cfg(feature = "atmega328p")]
pub mod memory {
  pub const FLASH_SIZE  : usize  = 32_768;
  pub const SRAM_SIZE   : usize  =  2_048;
  pub const SRAM_END    : usize  = 0x08FF;
  pub const EEPROM_SIZE : usize  =  1_024;

  pub mod alloc {
    pub const MIN_HEAPSIZE : usize = 768;
    pub const SMAX         : usize = 64;
  }

  pub mod eeprom {
    pub const EEPROM_SIZE      : usize = 1024;
  }
}

#[cfg(feature="atmega4809")]
pub mod oxide {
  pub const MAX_THREADS       : usize  = 4; // Ideally a power of 2, for efficiency
  pub const DEFAULT_MAIN_THREAD_STACK_SIZE : usize = 512;
  pub const DEFAULT_THREAD_STACK_SIZE : usize = 128;

  pub const PIN_ARRAY_SIZE    : usize = 48;
  pub const SERIAL_BUFFER     : usize = 32;
  pub const EVENT_QUEUE       : usize = 16;
}

#[cfg(feature="atmega328p")]
pub mod oxide {
  pub const MAX_THREADS       : usize  = 4;
  pub const DEFAULT_MAIN_THREAD_STACK_SIZE : usize = 384;
  pub const DEFAULT_THREAD_STACK_SIZE : usize = 64;

  pub const PIN_ARRAY_SIZE    : usize = 24;
  pub const SERIAL_BUFFER     : usize = 8;
  pub const EVENT_QUEUE       : usize = 8;
}


#[cfg(feature="20MHz")]
pub mod clock {
  pub const MASTER_CLOCK_HZ: u32         = 20_000_000; // Config'd in device fuses

  #[cfg(feature="power_low")]
  pub const MASTER_CLOCK_PRESCALER:  u8  = 8;
  #[cfg(feature="power_low")]
  pub const MASTER_TICK_FREQ_HZ: u16     = 50;

  #[cfg(feature="power_med")]
  pub const MASTER_CLOCK_PRESCALER:  u8  = 4;
  #[cfg(feature="power_med")]
  pub const MASTER_TICK_FREQ_HZ: u16     = 100;

  #[cfg(not(any(feature="power_med",feature="power_low")))]
  pub const MASTER_CLOCK_PRESCALER:  u8  = 1;
  #[cfg(not(any(feature="power_med",feature="power_low")))]
  pub const MASTER_TICK_FREQ_HZ: u16     = 200;
}

#[cfg(feature="16MHz")]
pub mod clock {
  pub const MASTER_CLOCK_HZ: u32         = 16_000_000; // Config'd in device fuses

  #[cfg(feature="power_low")]
  pub const MASTER_CLOCK_PRESCALER:  u8  = 8;
  #[cfg(feature="power_low")]
  pub const MASTER_TICK_FREQ_HZ: u16     = 40;

  #[cfg(feature="power_med")]
  pub const MASTER_CLOCK_PRESCALER:  u8  = 4;
  #[cfg(feature="power_med")]
  pub const MASTER_TICK_FREQ_HZ: u16     = 80;

  #[cfg(not(any(feature="power_med",feature="power_low")))]
  pub const MASTER_CLOCK_PRESCALER:  u8  = 1;
  #[cfg(not(any(feature="power_med",feature="power_low")))]
  pub const MASTER_TICK_FREQ_HZ: u16     = 160;
}


// Code ======================================================================

// Tests =====================================================================
