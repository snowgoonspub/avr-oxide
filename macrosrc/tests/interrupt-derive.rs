/* interrupt-derive.rs
 *
 * Developed by Tim Walls <tim.walls@snowgoons.com>
 * Copyright (c) All Rights Reserved, Tim Walls
 */

// Imports ===================================================================
#![feature(asm_sym)]
#![feature(asm_const)]
#![feature(naked_functions)]
#![feature(asm)]

// Declarations ==============================================================
#[oxide_macros::interrupt(isr="bobbins")]
pub fn my_interrupt_handler(mytoken: avr_oxide::concurrency::Isolated) {
  println!("This is my interrupt handler.")
}

// Code ======================================================================

// Tests =====================================================================
//#[test]
//fn test_interrupt_handler_symbols() {
//  let rust_isr : f() -> () = _isr_rust_bobbins;
//  let naked_isr: f() -> () = _naked_isr;
//}