/* persist.rs
 *
 * Developed by Tim Walls <tim.walls@snowgoons.com>
 * Copyright (c) All Rights Reserved, Tim Walls
 */
//! Derive macro to produce implementations of the Persist trait for
//! simple structures.



// Imports ===================================================================
use proc_macro2::Ident;
use darling::FromDeriveInput;
use proc_macro::TokenStream;
use syn::{parse_macro_input, DeriveInput, Data, DataStruct, Fields, DataEnum, Variant, Field};
use quote::{format_ident, quote};
use syn::punctuated::Punctuated;
use syn::token::Comma;

// Declarations ==============================================================
#[derive(FromDeriveInput)]
#[darling(attributes(persist))]
struct PersistOptions {
  #[darling(default)]
  magicnumber: Option<u8>
}

// Code ======================================================================
pub fn derive_persist(item: TokenStream) -> TokenStream {
  let input = parse_macro_input!(item as DeriveInput);
  let options = PersistOptions::from_derive_input(&input).expect("Inavlid Persist options");
  let type_name = input.ident;

  match input.data {
    Data::Struct(DataStruct {
                   fields: Fields::Named(fields),
                   ..
                 }) => {
      generate_for_struct(&type_name, &fields.named, &options)
    },
    Data::Struct(DataStruct {
      fields: Fields::Unnamed(fields),
      ..
                 }) => {
      generate_for_struct_anon(&type_name, &fields.unnamed, &options)
    },
    Data::Enum(DataEnum {
                 variants,
                 ..
               }) => {
      generate_for_enum(&type_name, &variants, &options)
    },
    _ => {
      panic!("#[derive(Persist)] only works for structs and enums");
    }
  }
}

fn generate_for_struct(type_name: &Ident, fields: &Punctuated<Field,Comma>, options: &PersistOptions) -> TokenStream {
  let magic_number = match options.magicnumber {
    None => { b'S' },
    Some(n) => n
  };

  // Create the set of load-field statements
  let load_fields = fields.iter().enumerate().map(move |(_i,field)|{
    let field_ident = field.ident.as_ref().unwrap();

    quote! {
      #field_ident: avr_oxide::util::persist::Persist::load_from(reader)?,
    }
  });

  // Create the set of save-field statements
  let save_fields = fields.iter().enumerate().map(move |(_i,field)|{
    let field_ident = field.ident.as_ref().unwrap();

    quote! {
      avr_oxide::util::persist::Persist::save_to(&self.#field_ident, writer)?;
    }
  });

  // Put it all together
  let output = quote! {
    impl avr_oxide::util::persist::Persist for #type_name {
      /// Load an instance of this struct from the given reader
      fn load_from<R: avr_oxide::io::Read>(reader: &mut R) -> avr_oxide::util::persist::PersistenceResult<Self> where Self: Sized {
        let magic_number: u8 = avr_oxide::util::persist::Persist::load_from(reader)?;

        if magic_number != #magic_number {
          avr_oxide::OxideResult::Err(avr_oxide::util::persist::PersistenceError::DeserialisationFailed)
        } else {
          avr_oxide::OxideResult::Ok(#type_name {
            #(#load_fields)*
          })
        }
      }

      /// Save an instance of this struct to the given writer.
      fn save_to<W: avr_oxide::io::Write>(&self, writer: &mut W) -> avr_oxide::util::persist::PersistenceResult<()> {
        avr_oxide::util::persist::Persist::save_to(&#magic_number as &u8, writer)?;
        #(#save_fields)*

        avr_oxide::OxideResult::Ok(())
      }
    }
  };
  output.into()
}

fn generate_for_struct_anon(type_name: &Ident, fields: &Punctuated<Field,Comma>, options: &PersistOptions) -> TokenStream {
  let magic_number = match options.magicnumber {
    None => { b'S' },
    Some(n) => n
  };

  // Create the set of load-field statements
  let load_fields = fields.iter().enumerate().map(move |(_i,_field)|{
    quote! {
      avr_oxide::util::persist::Persist::load_from(reader)?,
    }
  });

  // Create the set of save-field statements
  let save_fields = fields.iter().enumerate().map(move |(i,_field)|{

    let tuple_idx = syn::Index::from(i);

    quote! {
      avr_oxide::util::persist::Persist::save_to(&self.#tuple_idx, writer)?;
    }
  });

  // Put it all together
  let output = quote! {
    impl avr_oxide::util::persist::Persist for #type_name {
      /// Load an instance of this struct from the given reader
      fn load_from<R: avr_oxide::io::Read>(reader: &mut R) -> avr_oxide::util::persist::PersistenceResult<Self> where Self: Sized {
        let magic_number: u8 = avr_oxide::util::persist::Persist::load_from(reader)?;

        if magic_number != #magic_number {
          avr_oxide::OxideResult::Err(avr_oxide::util::persist::PersistenceError::DeserialisationFailed)
        } else {
          avr_oxide::OxideResult::Ok(#type_name (
            #(#load_fields)*
          ))
        }
      }

      /// Save an instance of this struct to the given writer.
      fn save_to<W: avr_oxide::io::Write>(&self, writer: &mut W) -> avr_oxide::util::persist::PersistenceResult<()> {
        avr_oxide::util::persist::Persist::save_to(&#magic_number as &u8, writer)?;
        #(#save_fields)*

        avr_oxide::OxideResult::Ok(())
      }
    }
  };
  output.into()
}

fn generate_for_enum(type_name: &Ident, variants: &Punctuated<Variant,Comma>, options: &PersistOptions) -> TokenStream {
  let magic_number = match options.magicnumber {
    None => { b'E' },
    Some(n) => n
  };

  // Create the set of load-field match arms
  let load_variants = variants.iter().enumerate().map(move |(i,variant)|{
    let variant_ident = &variant.ident;

    let fields_spec = match &variant.fields {

      Fields::Named(fields_named) => { // Named fields (struct basically)
        let load_fields = fields_named.named.iter().enumerate().map(move |(_i,field)|{
          let field_ident = field.ident.as_ref().unwrap();

          quote! {
            #field_ident: avr_oxide::util::persist::Persist::load_from(reader)?,
          }
        });

        quote! { { #(#load_fields)* } }
      },
      Fields::Unnamed(fields_unnamed) => { // Unnamed fields

        let load_fields = fields_unnamed.unnamed.iter().enumerate().map(move |(_i,_field)|{
          quote! {
            avr_oxide::util::persist::Persist::load_from(reader)?,
          }
        });

        quote! { ( #(#load_fields)* ) }
      },
      _=> { // No fields
        quote! {}
      }
    };


    quote! {
      #i => avr_oxide::OxideResult::Ok(#type_name::#variant_ident #fields_spec),
    }
  });

  // Create the set of save-field match arms
  let save_variants = variants.iter().enumerate().map(move |(i,variant)|{
    let variant_ident = &variant.ident;

    // First we need to construct the spec that will match this variant
    let match_spec = match &variant.fields {

      Fields::Named(fields_named) => { // Named fields (struct basically)
        let match_fields = fields_named.named.iter().enumerate().map(move |(_field_i,field)|{
          let field_ident = field.ident.as_ref().unwrap();
          let var_ident = format_ident!("field_{}", field_ident);
          quote! {
            #field_ident: #var_ident,
          }
        });

        quote! { { #(#match_fields)* } }
      },
      Fields::Unnamed(fields_unnamed) => { // Unnamed fields
        let match_fields = fields_unnamed.unnamed.iter().enumerate().map(move |(field_i,_field)|{
          let var_ident = format_ident!("field_{}", field_i);
          quote! {
            #var_ident,
          }
        });

        quote! { ( #(#match_fields)* ) }
      },
      _=> { // No fields
      quote! {}
      }
    };

    // And now create the code that will save the fields of this variant
    let save_fields = match &variant.fields {
      Fields::Named(fields_named) => { // Named fields
        let save_fields = fields_named.named.iter().enumerate().map(move |(_field_i,field)|{
          let field_ident = field.ident.as_ref().unwrap();
          let var_ident = format_ident!("field_{}", field_ident);
          quote! {
            avr_oxide::util::persist::Persist::save_to(#var_ident, writer)?;
          }
        });

        quote! { #(#save_fields)* }
      },
      Fields::Unnamed(fields_unnamed) => { // Unnamed fields
        let match_fields = fields_unnamed.unnamed.iter().enumerate().map(move |(field_i,_field)|{
          let var_ident = format_ident!("field_{}", field_i);
          quote! {
            avr_oxide::util::persist::Persist::save_to(#var_ident, writer)?;
          }
        });

        quote! { #(#match_fields)* }
      },
      _=> { // No fields
      quote! {}
      }
    };

    quote! {
      #type_name::#variant_ident #match_spec => {
        avr_oxide::util::persist::Persist::save_to(&#i as &usize, writer)?;
        #save_fields
      },
    }
  });


  let output = quote! {
    impl avr_oxide::util::persist::Persist for #type_name {
      /// Load an instance of this Enum from the given reader
      fn load_from<R: avr_oxide::io::Read>(reader: &mut R) -> avr_oxide::util::persist::PersistenceResult<Self> where Self: Sized {
        let magic_number: u8 = avr_oxide::util::persist::Persist::load_from(reader)?;

        if magic_number != #magic_number {
          avr_oxide::OxideResult::Err(avr_oxide::util::persist::PersistenceError::DeserialisationFailed)
        } else {
          let variant: usize = avr_oxide::util::persist::Persist::load_from(reader)?;

          match variant {
            #(#load_variants)*
            _ => avr_oxide::OxideResult::Err(avr_oxide::util::persist::PersistenceError::DeserialisationFailed)
          }
        }
      }

      /// Save an instance of this Enum to the given writer.
      fn save_to<W: avr_oxide::io::Write>(&self, writer: &mut W) -> avr_oxide::util::persist::PersistenceResult<()> {
        avr_oxide::util::persist::Persist::save_to(&#magic_number as &u8, writer)?;
        match self {
          #(#save_variants)*
        }
        avr_oxide::OxideResult::Ok(())
      }
    }
  };

  output.into()
}


// Tests =====================================================================
