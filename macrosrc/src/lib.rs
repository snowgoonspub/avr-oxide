#![feature(box_patterns)]
/* lib.rs
 *
 * Developed by Tim Walls <tim.walls@snowgoons.com>
 * Copyright (c) All Rights Reserved, Tim Walls
 */
//! AVRoxide procedural macros

// Imports ===================================================================
extern crate proc_macro;
use proc_macro::TokenStream;

mod entrypoint;
mod persist;
mod interrupt;

// Declarations ==============================================================
#[proc_macro_attribute]
#[cfg(not(test))]
pub fn main(args: TokenStream, item: TokenStream) -> TokenStream {
  entrypoint::generate_main(args,item)
}

#[proc_macro_derive(Persist, attributes(persist))]
pub fn derive_persist(item: TokenStream) -> TokenStream {
  persist::derive_persist(item)
}

#[proc_macro_attribute]
pub fn interrupt(args: TokenStream, item: TokenStream) -> TokenStream {
  interrupt::generate_interrupt(args,item)
}

// Code ======================================================================
fn parse_string_literal(string: syn::Lit) -> Result<String,syn::Error> {
  match string {
    syn::Lit::Str(s) => Ok(s.value()),
    syn::Lit::Verbatim(s) => Ok(s.to_string()),
    _ => Err(syn::Error::new_spanned(string, "Cannot parse value as string"))
  }
}

fn parse_usize_literal(literal: syn::Lit) -> Result<usize,syn::Error> {
  match literal {
    syn::Lit::Int(litint) => {
      Ok(litint.base10_parse::<usize>()?)
    },
    _ => Err(syn::Error::new_spanned(literal, "Cannot parse value as usize"))
  }
}



// Tests =====================================================================
