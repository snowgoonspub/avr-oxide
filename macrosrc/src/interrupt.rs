/* interrupt.rs
 *
 * Developed by Tim Walls <tim.walls@snowgoons.com>
 * Copyright (c) All Rights Reserved, Tim Walls
 */
//! Macros for generating interrupt service routines.

// Imports ===================================================================
use proc_macro::TokenStream;
use proc_macro2::Span;
use quote::{quote};
use syn::{FnArg, Pat, PatType, Ident};
use crate::parse_string_literal;

// Declarations ==============================================================
pub struct InterruptConfig {
  isr_name: String
}

// Code ======================================================================
pub fn generate_interrupt(args: TokenStream, item: TokenStream) -> TokenStream {
  let args = syn::parse_macro_input!(args as syn::AttributeArgs);
  let config = parse_args(args);

  match config {
    Ok(config) => {
      let input = syn::parse_macro_input!(item as syn::ItemFn);
      let rust_isr_name = Ident::new(&format!("__ivr_rust_{}", config.isr_name), Span::call_site());
      let naked_isr_name = Ident::new(&format!("_ivr_{}", config.isr_name), Span::call_site());
      let isr_body = &input.block;

      match (input.sig.inputs.len(), input.sig.inputs.first()) {
        (1, Some(FnArg::Typed(PatType{pat: box Pat::Ident(isotoken_arg), ..}))) => {
          let isotoken_name = &isotoken_arg.ident;
          quote! {
            #[no_mangle]
            #[naked]
            pub unsafe extern "C" fn #naked_isr_name() -> ! {
              #[cfg(target_arch="avr")]
              core::arch::asm!(
                "     call save_thread_context",
                // If context_flags.0 is set, we were just restored.
                // The jmp to 1f exits our ISR without executing it, thus we
                // need to skip it if the bit is clear
                "     sbic {context_flags_reg},0",
                "     jmp 1f",

                "     in r18,{sphregio}",
                "     in r19,{splregio}",

                "     ldi r20,hi8(__ISR_STACK_TOP)",
                "     ldi r21,lo8(__ISR_STACK_TOP)",

                "     out {splregio},r21",
                "     out {sphregio},r20",

                "     push r18",
                "     push r19",

                "     call {isr}",

                "     pop r19",
                "     pop r18",
                "     out {splregio},r19",
                "     out {sphregio},r18",

                "     jmp  restore_thread_context",
                "1:",
                // OK, at this point I've been restored - I may have got here
                // from a userland switch rather than an ISR.  I decide now
                // if I should return from interrupt or do a regular return
                "     sbic {context_flags_reg},2", // Tells us to enable ints
                "     sei",
                "     sbic {context_flags_reg},1", // Tells us to reti
                "     reti",
                "     ret",
                context_flags_reg = const(avr_oxide::hardware::cpu::cpuregs::IOADR_CONTEXT_FLAGS),
                sphregio = const(avr_oxide::hardware::cpu::cpuregs::IOADR_SPH),
                splregio = const(avr_oxide::hardware::cpu::cpuregs::IOADR_SPL),
                isr = sym #rust_isr_name,
                options(noreturn)
              );
              #[cfg(not(target_arch="avr"))]
              core::arch::asm!("nop",options(noreturn))
            }

            #[no_mangle]
            #[inline(never)]
            pub unsafe extern "C" fn #rust_isr_name() -> () {
              avr_oxide::concurrency::interrupt::isr(false, | #isotoken_name |
                #isr_body
              );
            }
          }.into()
        },
        _ => {
          syn::Error::new_spanned(&input.sig.ident, "ISRs must take exactly one parameter (`avr_oxide::concurrency::Isolated`)").to_compile_error().into()
        }
      }
    },
    Err(e) => {
      e.to_compile_error().into()
    }
  }
}

pub fn parse_args(args: syn::AttributeArgs) -> Result<InterruptConfig,syn::Error> {
  let mut isr:Option<String> = None;

  for arg in args {
    match arg {
      syn::NestedMeta::Meta(syn::Meta::NameValue(namevalue)) => {
        let key = namevalue.path.get_ident().unwrap().to_string().to_lowercase();
        match key.as_str() {
          "isr" => {
            isr = Some(parse_string_literal(namevalue.lit)?);
          },
          unknown => {
            return Err(syn::Error::new_spanned(namevalue, format!("Unknown attribute {}", unknown)));
          }
        }
      },
      syn::NestedMeta::Meta(syn::Meta::Path(path)) => {
        let key = path.get_ident().unwrap().to_string().to_lowercase();
        match key.as_str() {
          "isr" => {
            return Err(syn::Error::new_spanned(path, format!("The {} attribute requires a value", key)));
          },
          unknown => {
            return Err(syn::Error::new_spanned(path, format!("Unknown attribute {}", unknown)));
          }
        }
      },
      unknown => {
        return Err(syn::Error::new_spanned(unknown, "Unknown attribute"));
      }
    }
  }

  if isr.is_none() {
    return Err(syn::Error::new(Span::call_site(), "The `isr` name must be specified"));
  }

  Ok(InterruptConfig {
    isr_name: isr.unwrap()
  })
}

// Tests =====================================================================
