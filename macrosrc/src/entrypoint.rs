/* entrypoint.rs
 *
 * Developed by Tim Walls <tim.walls@snowgoons.com>
 * Copyright (c) All Rights Reserved, Tim Walls
 */
//! Procedural macros to generate the main entry point for an AVRoxide
//! application.


// Imports ===================================================================
use proc_macro::TokenStream;
use quote::quote;
use crate::{parse_string_literal,parse_usize_literal};

// Declarations ==============================================================
pub struct EntrypointConfig {
  _naked: bool,
  chip: String,
  stacksize: Option<usize>
}

// Code ======================================================================
pub fn generate_main(args: TokenStream, item: TokenStream) -> TokenStream {
  let args = syn::parse_macro_input!(args as syn::AttributeArgs);
  let config = parse_args(args);

  match config {
    Ok(config) => {
      let input = syn::parse_macro_input!(item as syn::ItemFn);

      // Naked main function; just do enough to hook into the bootloader
      // @todo in future I plan to make this optional, with the default being
      //       something a little fancier that also takes care of the Oxide
      //       event loop initialisation
      if input.sig.ident != "main" || !input.sig.inputs.is_empty() {
        return syn::Error::new_spanned(&input.sig.ident, "the main function must have the signature `fn main() -> !`").to_compile_error().into();
      }

      let bootlibname = format!("oxide-boot-{}", config.chip);
      let main_body = &input.block;


      let stacksize_ref = match config.stacksize {
        None => {
          quote! {
            static __OXIDE_MAIN_THREAD_STACK_SIZE: usize = avr_oxide::deviceconsts::oxide::DEFAULT_MAIN_THREAD_STACK_SIZE;
          }
        },
        Some(size_value) => {
          quote! {
            static __OXIDE_MAIN_THREAD_STACK_SIZE: usize = #size_value ;
          }
        }
      };

      let result = quote! {
        #[no_mangle]
        #stacksize_ref

        #[link(name=#bootlibname, kind="static")]
        extern {}
        #[no_mangle]
        pub fn __oxide_main() -> ! #main_body
      };
      result.into()
    },
    Err(e) => {
      e.to_compile_error().into()
    }
  }
}


pub fn parse_args(args: syn::AttributeArgs) -> Result<EntrypointConfig,syn::Error> {
  let mut naked:bool    = false;
  let mut stacksize:Option<usize> = None;
  let mut chip:Option<String> = None;

  for arg in args {
    match arg {
      syn::NestedMeta::Meta(syn::Meta::NameValue(namevalue)) => {
        let key = namevalue.path.get_ident().unwrap().to_string().to_lowercase();
        match key.as_str() {
          "stacksize" => {
            stacksize = Some(parse_usize_literal(namevalue.lit)?);
          },
          "chip" => {
            chip = Some(parse_string_literal(namevalue.lit)?);
          },
          unknown => {
            return Err(syn::Error::new_spanned(namevalue, format!("Unknown attribute {}", unknown)));
          }
        }
      },
      syn::NestedMeta::Meta(syn::Meta::Path(path)) => {
        let key = path.get_ident().unwrap().to_string().to_lowercase();
        match key.as_str() {
          "naked" => {
            naked = true;
          },
          "chip" |
          "stacksize" => {
            return Err(syn::Error::new_spanned(path, format!("The {} attribute requires a value", key)));
          },
          unknown => {
            return Err(syn::Error::new_spanned(path, format!("Unknown attribute {}", unknown)));
          }
        }
      },
      unknown => {
        return Err(syn::Error::new_spanned(unknown, "Unknown attribute"));
      }
    }
  }

  Ok(EntrypointConfig {
    _naked: naked,
    chip: chip.unwrap(),
    stacksize
  })
}

// Tests =====================================================================
