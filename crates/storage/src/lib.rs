/* lib.rs
 *
 * Developed by Tim Walls <tim.walls@snowgoons.com>
 * Copyright (c) All Rights Reserved, Tim Walls
 */
//! Storage device drivers for the [AVRoxide] operating system.
//!
//! # Features
//! ## 32 (default), 16 or 64 bit maximum file sizes
//! By default, the maximum size for files is determined by the 32-bit
//! values used for offset addresses.  This is more than adequate for
//! almost any storage realistically attached to an Arduino - but, I guess
//! it is possible that you could attach, say, an SD-Card with greated than
//! 4GB.  You may optionally pass a feature flag to enable 64 bit values
//! therefore.
//!
//! Similarly, if you know the largest file you will use is 64k or less,
//! you can specify 16bit filesize instead.
//!
//! Smaller maximum filesizes reduces memory use, but also critically may
//! mean you avoid having to import 64bit maths functions, reducing codesize
//! as well.
//!
//! | Flag | Meaning |
//! | ---- | ------- |
//! | `filesize_16bit` | Maximum filesize is 16 bits |
//! | `filesize_32bit` | (Default) maximum filesize is 32 bits |
//! | `filesize_64bit` | Maximum filesize is 64 bits |
//!
//! [AVRoxide]: https://avroxi.de/

#![cfg_attr(target_arch="avr", no_std)]

extern crate self as avrox_storage;

pub type SAddr = u32;
pub type SSize = u32;

#[cfg(feature="filesize_32bit")]
mod variabletypes {
  pub type FileAddr  = u32;
  pub type FileOffset = i32;
}

#[cfg(feature="filesize_16bit")]
mod variabletypes {
  pub type FileAddr  = u16;
  pub type FileOffset = i16;
}

#[cfg(feature="filesize_64bit")]
mod variabletypes {
  pub type FileAddr  = u64;
  pub type FileOffset = i64;
}

pub use variabletypes::FileAddr;
pub use variabletypes::FileOffset;


pub mod fs;
pub mod serprom;
mod buffered;
mod random;
mod seek;

pub use random::RandomRead;
pub use random::RandomWrite;
pub use buffered::PageBuffer;
pub use seek::{SeekFrom,Seek};

