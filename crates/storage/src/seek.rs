/* seek.rs
 *
 * Developed by Tim Walls <tim.walls@snowgoons.com>
 * Copyright (c) All Rights Reserved, Tim Walls
 */
//! Trait for seekable storage devices
//!
//!

// Imports ===================================================================
use avrox_storage::{FileAddr, FileOffset};
use avr_oxide::OxideResult::Ok;

// Declarations ==============================================================
/// Possible methods to seek within an I/O object
#[derive(Copy,Clone,Eq,PartialEq)]
pub enum SeekFrom {
  /// Given number of bytes from start
  Start(FileAddr),
  /// Given number of bytes from the end
  End(FileOffset),
  /// Given number of bytes from the current position
  Current(FileOffset)
}


pub trait Seek {
  fn seek(&mut self, pos: SeekFrom) -> avr_oxide::io::Result<FileAddr>;
  fn rewind(&mut self) -> avr_oxide::io::Result<()> {
    self.seek(SeekFrom::Start(FileAddr::MIN))?;
    Ok(())
  }
  fn stream_position(&mut self) -> avr_oxide::io::Result<FileAddr>;
}

// Code ======================================================================

// Tests =====================================================================
