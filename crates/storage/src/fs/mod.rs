/* mod.rs
 *
 * Developed by Tim Walls <tim.walls@snowgoons.com>
 * Copyright (c) All Rights Reserved, Tim Walls
 */
//! Filesystem module for AVRoxide.

// Imports ===================================================================
mod snafus;
mod filesystem;
mod file;

// Declarations ==============================================================
pub use avr_oxide::io::{ Read, Write, IoError };
pub use snafus::{FileUid,SnafusFileSystem};
pub use file::{File,OpenOptions};
pub use filesystem::{FileSystemRead, FileSystemWrite, FileSystemResult};

// Code ======================================================================

// Tests =====================================================================
