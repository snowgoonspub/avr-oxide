/* serprom/mod.rs
 *
 * Developed by Tim Walls <tim.walls@snowgoons.com>
 * Copyright (c) All Rights Reserved, Tim Walls
 */
//! Serial (EE)PROM drivers
//!
//! | Device      | Manufacturer | Specification |
//! | ----------- | ------------ | -------------------- |
//! | BR24T1M_3AM | Rohm         | 128Kx8 Serial EEPROM[^1] |
//!
//! [^1]: Note; currently only the first 64K is addressable
//!
//! # Usage example
//! ```no_run
//! # #![no_std]
//! # #![no_main]
//! #
//! # use avr_oxide::hal::generic::twi::{TwoWireMaster, TwiAddr, InterfaceMode, PortSpeed, RetryStrategy, Command};
//! # use avr_oxide::io::{IoError, Read};
//! # use avr_oxide::devices::{ OxideSerialBus };
//! # use avr_oxide::{print, println};
//! # use avr_oxide::StaticWrap;
//! use avr_oxide::hardware;
//! use avrox_storage::{serprom,RandomRead};
//! use avr_oxide::devices::serialbus::UsesSerialBusClient;
//!
//! #[avr_oxide::main(chip="atmega4809",stacksize=512)]
//! pub fn main() {
//!   let supervisor = avr_oxide::oxide::instance();
//!
//!   let bus = StaticWrap::new(OxideSerialBus::using_bus(hardware::twi::twi0::instance().mux(hardware::twi::twi0::TwiPins::MasterASlaveC).mode(InterfaceMode::I2C, PortSpeed::Fast)));
//!   supervisor.listen(bus.borrow());
//!   let serprom = serprom::BR24T1M_3AM::using_client(OxideSerialBus::client(bus.borrow(), TwiAddr::addr(0xA0)));
//!
//!   let mut buffer = [ 0x00u8, 0x00u8, 0x00u8, 0x00u8 ];
//!   serprom.read_at(0u32, &mut buffer);
//! #
//! # supervisor.run()
//! }
//! ```

pub mod generic;
pub mod composite;

#[allow(non_camel_case_types)]
/// ROHM Semiconductor: BR24T1M-3AM is a serial EEPROM of I2C BUS Interface Method
pub type BR24T1M_3AM<BC> = composite::CompositeSerProm<2,0b00000010,1,BC,generic::SerPromD8A16be<65_536,256,BC>>;
