/* random.rs
 *
 * Developed by Tim Walls <tim.walls@snowgoons.com>
 * Copyright (c) All Rights Reserved, Tim Walls
 */
//! Traits for random-access read/write to storage.

// Imports ===================================================================
use avr_oxide::util::datatypes::{BitField, BitFieldAccess, BitIndex};
use avrox_storage::{SAddr, SSize };
use avr_oxide::OxideResult::{Ok,Err};

// Declarations ==============================================================
/// Trait implemented by anything we can reasonably describe as Storage
pub trait Storage {
  const ADDRESSABLE_BYTES: SSize;

  /// Return the total number of bytes addressable by this device
  fn addressable_bytes() -> SSize {
    Self::ADDRESSABLE_BYTES
  }
}

/// Access hints that can be used to tell the storage driver how to
/// optimise reads/writes
#[derive(Copy,Clone,PartialEq,Eq)]
pub struct AccessHint(BitField);

/// Trait implemented by drivers that allow random-access read.
pub trait RandomRead {
  /// Read into the given buffer, from the given device address.
  ///
  /// If succesful, the number of bytes actually read will be returned in
  /// the Ok() result.
  ///
  #[must_use = "the buffer is not guaranteed to be filled; the Result will indicate how many bytes were actually read"]
  fn read_at_hinted(&self, addr: SAddr, buf: &mut [u8], hint: AccessHint) -> avr_oxide::io::Result<usize>;

  /// Read into the given buffer, from the given device address.
  ///
  /// If succesful, the number of bytes actually read will be returned in
  /// the Ok() result.
  ///
  #[must_use = "the buffer is not guaranteed to be filled; the Result will indicate how many bytes were actually read"]
  fn read_at(&self, addr: SAddr, buf: &mut [u8]) -> avr_oxide::io::Result<usize> {
    self.read_at_hinted(addr, buf, AccessHint::default())
  }


  /// Read into the given buffer, from the given device address.
  /// The entire buffer will be filled unless an error condition occurs (e.g.
  /// attempting to read off the end of device memory).  If an error does
  /// occur, the state (partially written, not written at all) of the buffer
  /// is undefined.
  fn read_full_at_hinted(&self, addr: SAddr, buf: &mut [u8], hint: AccessHint) -> avr_oxide::io::Result<()> {
    let mut start = 0usize;

    while start < buf.len() {
      match self.read_at_hinted(addr + start as SAddr, &mut buf[start..], hint) {
        Ok(0) => {
          return Err(avr_oxide::io::IoError::EndOfFile)
        },
        Ok(bytes) => {
          start += bytes;
        },
        Err(e) => {
          return Err(e)
        }
      }
    }
    Ok(())
  }

  /// Read into the given buffer, from the given device address.
  /// The entire buffer will be filled unless an error condition occurs (e.g.
  /// attempting to read off the end of device memory).  If an error does
  /// occur, the state (partially written, not written at all) of the buffer
  /// is undefined.
  fn read_full_at(&self, addr: SAddr, buf: &mut [u8]) -> avr_oxide::io::Result<()> {
    self.read_full_at_hinted(addr, buf, AccessHint::default())
  }
}

/// Trait implemented by drivers that allow random-access write.
pub trait RandomWrite {
  /// Write the contents of the given buffer at the given device address.
  /// THe entire buffer will be written unless an error condition occurs.
  /// If an error does occur, the state (partially written, not written at all)
  /// is undefined.
  fn write_all_at_hinted(&mut self, addr: SAddr, buf: &[u8], hint: AccessHint) -> avr_oxide::io::Result<()> {

    let mut start = 0usize;

    while start < buf.len() {
      match self.write_at_hinted(addr + start as SAddr, &buf[start..], hint) {
        Ok(0) => {
          return Err(avr_oxide::io::IoError::NoFreeSpace)
        },
        Ok(bytes) => {
          start += bytes;
        },
        Err(e) => {
          return Err(e)
        }
      }
    }
    Ok(())
  }

  /// Write the contents of the given buffer at the given device address.
  /// THe entire buffer will be written unless an error condition occurs.
  /// If an error does occur, the state (partially written, not written at all)
  /// is undefined.
  fn write_all_at(&mut self, addr: SAddr, buf: &[u8]) -> avr_oxide::io::Result<()> {
    self.write_all_at_hinted(addr, buf, AccessHint::default())
  }

    /// Write the contents of the given buffer at the given device address.
  /// Depending on the nature of the underlying device, the write may not
  /// complete.  The number of bytes actually written will be returned in the
  /// Ok() result, and it is the caller's responsibility to resume writing
  /// the remainder.
  #[must_use = "the entire buffer is not guaranteed to be written; the Result will indicate how many bytes were actually written"]
  fn write_at_hinted(&mut self, addr: SAddr, buf: &[u8], hint: AccessHint) -> avr_oxide::io::Result<usize>;

  /// Write the contents of the given buffer at the given device address.
  /// Depending on the nature of the underlying device, the write may not
  /// complete.  The number of bytes actually written will be returned in the
  /// Ok() result, and it is the caller's responsibility to resume writing
  /// the remainder.
  #[must_use = "the entire buffer is not guaranteed to be written; the Result will indicate how many bytes were actually written"]
  fn write_at(&mut self, addr: SAddr, buf: &[u8]) -> avr_oxide::io::Result<usize> {
    self.write_at_hinted(addr, buf, AccessHint::default())
  }

  /// Flush the write to the device.  Writes are not guaranteed to have been
  /// completed unless this method has been called.
  fn flush(&mut self) -> avr_oxide::io::Result<()>;
}
// Code ======================================================================
const HINT_WRITEONLY:     BitIndex = BitIndex::bit_c(0);
const HINT_NONSEQUENTIAL: BitIndex = BitIndex::bit_c(1);

impl AccessHint {
  pub const WRITEONLY:     AccessHint =  AccessHint(BitField::with_initial(0b00000001));
  pub const NONSEQUENTIAL: AccessHint =  AccessHint(BitField::with_initial(0b00000010));

  /// Set the writeonly hint.  This tells the storage driver that the
  /// bytes written are unlikely to be read back any time soon.
  pub fn writeonly(mut self) -> Self {
    self.0.set(HINT_WRITEONLY);
    self
  }

  /// Set the non-sequential hint.  This tells the storage driver that
  /// we are not reading/or writing in a nice sequential order.
  pub fn nonsequential(mut self) -> Self {
    self.0.set(HINT_NONSEQUENTIAL);
    self
  }

  pub fn is_writeonly(&self) -> bool {
    self.0.is_set(HINT_WRITEONLY)
  }

  pub fn is_nonsequential(&self) -> bool {
    self.0.is_set(HINT_NONSEQUENTIAL)
  }
}

impl Default for AccessHint {
  fn default() -> Self {
    AccessHint(BitField::all_clr())
  }
}

// Tests =====================================================================
