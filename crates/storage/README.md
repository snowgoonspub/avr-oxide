# AVRox-Storage
Storage device drivers for the [AVRoxide] AVR operating system

# Supported Devices
* Rohm BR24T1M-3AM serial 128Kx8 EEPROM

# Driver Overlays
* `PageBuffer` - buffered overlay that can wrap any device storage driver

# File System Overlays
* `SNaFus` - simple numbered filing system overlay that gives traditional 
  open/seek/read/write API for storage devices.

# Documentation
See the main AVRoxide documentation at [https://avroxi.de/categories/gettingstarted/](https://avroxi.de/categories/gettingstarted/)


