/* mod.rs
 *
 * Developed by Tim Walls <tim.walls@snowgoons.com>
 * Copyright (c) All Rights Reserved, Tim Walls
 */
//! Graphics primitives for our display driver layer.  The general philosophy
//! we are taking is that we have far more CPU cycles spare than memory on
//! a chip like an AVR.  So rather than keeping a bitmap buffer and writing to
//! it, we take the approach of not holding any buffer and instead
//! implement primitives that can calculate the appropriate value for any
//! given pixel on-demand.

// Imports ===================================================================
use avrox_display::GfxResult;
pub mod primitives;
pub mod pixels;
mod patterns;
pub mod sevenseg;
pub mod dynamic;
pub mod img;
pub mod fills;

#[cfg(test)]
mod test;

use avr_oxide::OxideResult::{Ok};

// Declarations ==============================================================
/// Hints that tell a component the order in which a device will typically
/// ask for pixel data.  This may be used by the graphics command to
/// optimise how it retrieves, calculates or caches data.
#[cfg_attr(not(target_arch="avr"), derive(Debug))]
#[cfg_attr(target_arch="avr", derive(ufmt::derive::uDebug))]
pub enum RenderOrderingHint {
  /// The device typically renders row-by-row
  Rows,
  /// The device typically renders column-by-column
  Columns,
  /// No device follows no rhyme or reason
  Random
}



/// X-coordinates
pub type XCoord = u16;
/// Y-coordinates
pub type YCoord = u16;

/// An (X,Y) coordinate pair.
#[cfg_attr(not(target_arch="avr"), derive(Debug))]
#[cfg_attr(target_arch="avr", derive(ufmt::derive::uDebug))]
#[derive(Copy,Clone,Eq,PartialEq)]
pub struct Point(pub XCoord, pub YCoord);

#[cfg_attr(not(target_arch="avr"), derive(Debug,PartialEq))]
#[cfg_attr(target_arch="avr", derive(ufmt::derive::uDebug))]
pub struct Area {
  pub(crate) tl: Point,
  pub(crate) w: XCoord,
  pub(crate) h: YCoord
}

pub trait RenderPlane {
  const WIDTH : XCoord;
  const HEIGHT : YCoord;
  const ORDERHINT: RenderOrderingHint;
}

/// Trait implemented by any graphics command that is capable of being
/// rendered onto a 2d plane
pub trait Renderable {
  type PIXEL;

  /// Return the value of the pixel at the given X-Y coordinate
  fn get_pixel_at<P: RenderPlane>(&self, coord: Point) -> GfxResult<Self::PIXEL>;

  /// Return the dimensions of this primitive.  Other container types may
  /// use this to aid in optimisation or layout.
  fn get_dimensions<P: RenderPlane>(&self) -> GfxResult<(XCoord,YCoord)> {
    Ok(( P::WIDTH, P::HEIGHT ))
  }

  /// Indicate if any of the content of this renderable has (or may have)
  /// changed.
  fn has_changes<P: RenderPlane>(&self) -> bool {
    true
  }

  /// Return the area of this renderable that has (or may have) changed.
  fn get_change_area<P: RenderPlane>(&self) -> GfxResult<Option<Area>> {
    if self.has_changes::<P>() {
      // Default implementation is just to return our entire area; this will
      // always be 'correct' (it 'may' have changed) even if inefficient
      let dims = self.get_dimensions::<P>()?;

      Ok(Some(Area {
        tl: Point(0, 0),
        w: dims.0,
        h: dims.1
      }))
    } else {
      Ok(None)
    }
  }
}


// Code ======================================================================

/// Merge two areas into a new one which encompasses both the original areas.
fn merge_areas(area1: Option<Area>, area2: Option<Area>) -> Option<Area> {
  #[cfg(test)]
  println!("  Merging {:?} with {:?}", &area1, &area2);

  let merged = match (area1, area2) {
    (None, None)          => None,
    (Some(a), None) => Some(a),
    (None, Some(b)) => Some(b),
    (Some(a), Some(b)) => {
      let ( tl_a_x, tl_a_y ) = ( a.tl.0, a.tl.1 );
      let ( tl_b_x, tl_b_y ) = ( b.tl.0, b.tl.1 );

      let ( br_a_x, br_a_y ) = ( tl_a_x + a.w, tl_a_y + a.h );
      let ( br_b_x, br_b_y ) = ( tl_b_x + b.w, tl_b_y + b.h );

      let tl_x = core::cmp::min(tl_a_x, tl_b_x);
      let tl_y = core::cmp::min(tl_a_y, tl_b_y);

      let br_x = core::cmp::max(br_a_x, br_b_x);
      let br_y = core::cmp::max(br_a_y, br_b_y);

      let w = br_x - tl_x;
      let h = br_y - tl_y;

      Some(Area {
        tl: Point(tl_x, tl_y),w,h
      })
    }
  };

  #[cfg(test)]
  println!("    ==> {:?}", &merged);

  merged
}

// Tests =====================================================================
#[cfg(test)]
mod tests {
  use avrox_display::gfx::{Area, XCoord, Point, YCoord, merge_areas};

  fn area(tlx: XCoord, tly: YCoord, w: XCoord, h: YCoord) -> Area {
    Area {
      tl: Point(tlx, tly),
      w,
      h
    }
  }

  #[test]
  fn test_area_merges() {
    let area1 = Some(area(0,0,24,7));
    let area2 = Some(area(24, 0, 72, 14));

    assert_eq!(merge_areas(area1,area2), Some(area(0,0,24+72,14)));

  }
}