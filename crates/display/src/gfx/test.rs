/* test.rs
 *
 * Developed by Tim Walls <tim.walls@snowgoons.com>
 * Copyright (c) All Rights Reserved, Tim Walls
 */
//! Test renderer implementation
//!
//!

// Imports ===================================================================

use avrox_display::gfx::pixels::{Grey, Monochromatic};
use avrox_display::gfx::{Renderable, RenderOrderingHint, RenderPlane, GfxResult, XCoord, Point, YCoord};
use avr_oxide::OxideResult::{Ok,Err};

// Declarations ==============================================================
pub(crate) struct MonochromeTestRenderer {
}

pub(crate) struct GreyscaleTestRenderer {
}

// Code ======================================================================
impl RenderPlane for MonochromeTestRenderer {
  const WIDTH: XCoord = 96;
  const HEIGHT: YCoord = 16;
  const ORDERHINT: RenderOrderingHint = RenderOrderingHint::Rows;
}

impl RenderPlane for GreyscaleTestRenderer {
  const WIDTH: XCoord = 96;
  const HEIGHT: YCoord = 16;

  const ORDERHINT: RenderOrderingHint = RenderOrderingHint::Columns;
}

impl MonochromeTestRenderer {
  pub(crate) fn new() -> Self {
    MonochromeTestRenderer {
    }
  }

  pub(crate) fn render_scene<RENDERABLE>(&self, object: &RENDERABLE)
  where
    RENDERABLE: Renderable<PIXEL=Monochromatic>
  {
    println!("\nDisplay output:");
    for y in 0..Self::HEIGHT {
      for x in 0..Self::WIDTH {
        match object.get_pixel_at::<Self>(Point(x as XCoord, y as YCoord)) {
          Ok(pixel) => {
            if pixel.is_set() {
              print!("XX");
            } else {
              print!("  ");
            }
          },
          Err(_) => {
            print!("??");
          }
        }
      }
      println!("");
    }
  }
}

impl GreyscaleTestRenderer {
  pub(crate) fn new() -> Self {
    GreyscaleTestRenderer {
    }
  }

  pub(crate) fn render_scene<RENDERABLE>(&self, object: &RENDERABLE)
    where
      RENDERABLE: Renderable<PIXEL=Grey>
  {
    println!("\nDisplay output:");
    for y in 0..Self::HEIGHT {
      for x in 0..Self::WIDTH {
        match object.get_pixel_at::<Self>(Point(x as XCoord, y as YCoord)) {
          Ok(pixel) => {
            print!("{}", match pixel.get_value()/26 {
              0 => "  ",
              1 => "..",
              2 => "::",
              3 => "--",
              4 => "==",
              5 => "++",
              6 => "**",
              7 => "##",
              8 => "%%",
              9 => "@@",
              _ => "**"

            });
          },
          Err(_) => {
            print!("//");
          }
        }
      }
      println!("");
    }
  }
}


// Tests =====================================================================
#[cfg(test)]
mod tests {
  use avrox_display::gfx::fills::{crosshatch_fill, SolidFill};
  use avrox_display::gfx::pixels::{Grey, Monochromatic};
  use avrox_display::gfx::primitives::{Overlay, Rectangle};
  use avrox_display::gfx::test::{GreyscaleTestRenderer, MonochromeTestRenderer};

  #[test]
  fn test_monochrome_rendering() {
    let renderer = MonochromeTestRenderer::new();
    let scene =
      Overlay::new(
        Rectangle::new((10, 5),
                       Monochromatic::WHITE,
                       SolidFill::new(Monochromatic::BLACK)),
        crosshatch_fill());

    renderer.render_scene(&scene);
  }


  #[test]
  fn test_greyscale_rendering() {
    let renderer = GreyscaleTestRenderer::new();
    let scene =
      Overlay::new(
        Rectangle::new((30, 6),
                       Grey::WHITE,
                       SolidFill::new(Grey::GREY25PC)),
        crosshatch_fill());

    renderer.render_scene(&scene);
  }

}