/* dynamic.rs
 *
 * Developed by Tim Walls <tim.walls@snowgoons.com>
 * Copyright (c) All Rights Reserved, Tim Walls
 */
//! Dynamic data binding for the display layer

// Imports ===================================================================
use core::cell::RefCell;

// Declarations ==============================================================
/// Trait implemented by things that we want to be able to display values of
pub trait DisplayAs<X> {
  fn display_value(&self) -> X;
  /// True if this value is mutable (i.e. can change underneath us :-).)
  fn is_mutable(&self) -> bool;
}

pub trait OptionalDisplayAs<X> {
  fn is_displayed(&self) -> bool;
  fn display_value(&self) -> X;
  /// True if this value is mutable (i.e. can change underneath us :-).)
  fn is_mutable(&self) -> bool;
}

// Code ======================================================================
impl<X> DisplayAs<X> for X
where
  X: Clone
{
  fn display_value(&self) -> X {
    self.clone()
  }

  fn is_mutable(&self) -> bool {
    false
  }
}

impl<X,Y> OptionalDisplayAs<X> for Option<Y>
where
  X: Default,
  Y: DisplayAs<X>
{
  fn is_displayed(&self) -> bool {
    self.is_some()
  }

  fn display_value(&self) -> X {
    match self {
      Some(v) => {
        v.display_value()
      },
      None => {
        X::default()
      }
    }
  }

  fn is_mutable(&self) -> bool {
    match self {
      Some(v) => {
        v.is_mutable()
      },
      None => {
        false
      }
    }
  }
}

impl<X> DisplayAs<X> for &RefCell<X>
where
  X: Clone
{
  fn display_value(&self) -> X {
    (*self).borrow().clone()
  }

  fn is_mutable(&self) -> bool {
    true
  }
}

impl<X,Y> OptionalDisplayAs<Y> for &RefCell<X>
where
  X: OptionalDisplayAs<Y>
{
  fn is_displayed(&self) -> bool {
    (*self).borrow().is_displayed()
  }

  fn display_value(&self) -> Y {
    (*self).borrow().display_value()
  }

  fn is_mutable(&self) -> bool {
    true
  }
}


// Tests =====================================================================
