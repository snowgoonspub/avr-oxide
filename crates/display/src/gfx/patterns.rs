/* patterns.rs
 *
 * Developed by Tim Walls <tim.walls@snowgoons.com>
 * Copyright (c) All Rights Reserved, Tim Walls
 */
//! @todo documentation for this module
//!
//!

// Imports ===================================================================
use avrox_display::gfx::{GfxResult, Point};
use avrox_display::gfx::pixels::Monochromatic;
use avr_oxide::OxideResult::Ok;

// Declarations ==============================================================



// Code ======================================================================
pub fn vertical_pinstripe(coord: Point) -> GfxResult<Monochromatic> {
  if (coord.0 & 0b00000001) > 0 {
    Ok(Monochromatic::WHITE)
  } else {
    Ok(Monochromatic::BLACK)
  }
}

pub fn horizontal_pinstripe(coord: Point) -> GfxResult<Monochromatic> {
  if (coord.1 & 0b00000001) > 0 {
    Ok(Monochromatic::WHITE)
  } else {
    Ok(Monochromatic::BLACK)
  }
}

pub fn crosshatch(coord: Point) -> GfxResult<Monochromatic> {
  if (coord.0 & 0b00000001) > 0 {
    if (coord.1 & 0b00000001) > 0 {
      Ok(Monochromatic::WHITE)
    } else {
      Ok(Monochromatic::BLACK)
    }
  } else {
    if (coord.1 & 0b00000001) > 0 {
      Ok(Monochromatic::BLACK)
    } else {
      Ok(Monochromatic::WHITE)
    }
  }
}



// Tests =====================================================================
