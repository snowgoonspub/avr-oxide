/* displayvisions.rs
 *
 * Developed by Tim Walls <tim.walls@snowgoons.com>
 * Copyright (c) All Rights Reserved, Tim Walls
 */
//! DisplayVisions displays

use avr_oxide::hal::generic::twi::TwiAddr;
// Imports ===================================================================
use avrox_display::drivers::solomon;
use avrox_display::drivers::solomon::{PixelByteOrder, StartLineCmdType};
use avrox_display::gfx::pixels::Monochromatic;

// Declarations ==============================================================
pub struct DVEAW096016DriverConfig;

pub type DisplayVisionsEAW096016<CLIENT> =
solomon::SolomonDisplay<DVEAW096016DriverConfig,CLIENT,Monochromatic>;

// Code ======================================================================
impl solomon::GenericConfig for DVEAW096016DriverConfig {
  const CP_VOLTAGE: u8  = solomon::CP_6V;
  const COL_ORDER: u8   = solomon::COL_RIGHTTOLEFT;
  const SEG_ORDER: u8   = solomon::SEG_SEG0COL127;
  const MPLEX_RATIO: u8 = 0x0F;
  const WIDTH: usize    = 96;
  const HEIGHT: usize   = 16;
  const RAMWIDTH_BYTES: usize = 16;
  const RAMHEIGHT_BYTES: usize = 8;
  const PIXEL_ORDER: PixelByteOrder = PixelByteOrder::LsbTop;
  const DEFAULT_I2C: TwiAddr = TwiAddr::addr_7bit(0x3C);
  const STARTLINE_CMD_TYPE: StartLineCmdType = StartLineCmdType::SingleByte0x40;
  const SET_PAGE_COL_CMD: u8 = 0x21;
  const SET_PAGE_ROW_CMD: u8 = 0x22;
}



// Tests =====================================================================
