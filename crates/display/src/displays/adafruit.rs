/* adafruit.rs
 *
 * Developed by Tim Walls <tim.walls@snowgoons.com>
 * Copyright (c) All Rights Reserved, Tim Walls
 */
//! Adafruit display modules

use avr_oxide::hal::generic::twi::TwiAddr;
// Imports ===================================================================
use avrox_display::drivers::solomon;
use avrox_display::drivers::solomon::{PixelByteOrder, StartLineCmdType};
use avrox_display::gfx::pixels::Grey;


// Declarations ==============================================================
pub struct Greyscale128x128DriverConfig;

pub type AdafruitGreyscale128x128<CLIENT> =
solomon::SolomonDisplay<Greyscale128x128DriverConfig,CLIENT, Grey>;

// Code ======================================================================
impl solomon::GenericConfig for Greyscale128x128DriverConfig {
  const CP_VOLTAGE: u8  = solomon::CP_6V;
  const COL_ORDER: u8   = solomon::COL_RIGHTTOLEFT;
  const SEG_ORDER: u8   = solomon::SEG_SEG0COL127;
  const MPLEX_RATIO: u8 = 0x7f;
  const WIDTH: usize    = 128;
  const HEIGHT: usize   = 128;
  const PIXEL_ORDER: PixelByteOrder = PixelByteOrder::LsbLeft;
  const DEFAULT_I2C: TwiAddr = TwiAddr::addr_7bit(0x3D);
  const STARTLINE_CMD_TYPE: StartLineCmdType = StartLineCmdType::DoubleByteA1;

  const RESET_CMD: &'static [u8] = &[
    0x00u8,
    0xFD, 0x12, // Unlock OLED interface
    0xA8, Self::MPLEX_RATIO,
//    0xA0, 0x51, // Col addr remap, COM remap, COM odd/even
    0xA0, 0b01010011,
    0xAB, 0x01, // Enable internal regulator
    0xB6, 0x04, // Precharge of 4 clocks
    0xBE, 0x0F, // COM deselect voltage level
    0xBC, 0x08, // Pre-charge voltage level
    0xD5, 0x62, // Enable second precharge, internal VSL
    0xB1, 0x11, // Set phase length,
    0xA2, 0x00, // Set display offset
    0xB3, 0x00, // Set display clock
    0xA4,       // Normal display mode
  ];

  const SET_PAGE_COL_CMD: u8 = 0x15;
  const SET_PAGE_ROW_CMD: u8 = 0x75;
  const RAMWIDTH_BYTES: usize = 64;
  const RAMHEIGHT_BYTES: usize = 16;
}


// Tests =====================================================================
