/* lib.rs
 *
 * Developed by Tim Walls <tim.walls@snowgoons.com>
 * Copyright (c) All Rights Reserved, Tim Walls
 */
//! Display device drivers for the [AVRoxide] operating system.
//!
//! [AVRoxide]: https://avroxi.de/
#![feature(asm_experimental_arch)]

#![cfg_attr(target_arch="avr", no_std)]

extern crate self as avrox_display;

use avr_oxide::hal::generic::twi::TwiError;
use avr_oxide::io::IoError;
use avr_oxide::OxideResult;
use avr_oxide::util::persist::PersistenceError;

pub mod gfx;
pub mod drivers;
pub mod displays;
mod displaydevice;

pub use displaydevice::{DisplayDevice,PowerLevel};

/// Errors that might be returned by the graphics subsystem
#[cfg_attr(not(target_arch="avr"), derive(Debug))]
#[cfg_attr(target_arch="avr", derive(ufmt::derive::uDebug))]
pub enum GfxError {
  /// The coordinates that were given were out of bounds
  OutOfBounds,

  /// An IO error occurred
  IoError(IoError),

  /// An error communicating with the hardware device occurred
  I2CBusError(TwiError),

  /// An error with persistent data source
  PersistenceError(PersistenceError),

  /// A bad file format was provided
  FileFormat,

  /// The requested layer type was not found in the given file
  MissingLayer,

  /// The requested functionality is not supported by the given resource
  /// (typically, you asked for a type of image from a file format that
  /// can't provide it)
  NotSupported
}

pub type GfxResult<R> = OxideResult<R, GfxError>;

impl From<IoError> for GfxError {
  fn from(e: IoError) -> Self {
    GfxError::IoError(e)
  }
}

impl From<TwiError> for GfxError {
  fn from(e: TwiError) -> Self {
    GfxError::I2CBusError(e)
  }
}

impl From<PersistenceError> for GfxError {
  fn from(e: PersistenceError) -> Self {
    GfxError::PersistenceError(e)
  }
}
