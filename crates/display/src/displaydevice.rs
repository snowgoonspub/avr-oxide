/* displaydevice.rs
 *
 * Developed by Tim Walls <tim.walls@snowgoons.com>
 * Copyright (c) All Rights Reserved, Tim Walls
 */
//! Generic interface to display devices.

// Imports ===================================================================
use avrox_display::gfx::Renderable;
use avrox_display::GfxResult;

// Declarations ==============================================================
/// Displays may have a variety of power levels - from full power to total
/// hibernation.  These generic levels will map to different feature sets
/// in different devices, but the general principle of each level should
/// be maintained.
#[allow(dead_code)]
pub enum PowerLevel {
  /// The lowest level of power use.  Everything should be switched off as
  /// far as possible, potentially including display RAM.  (i.e. it is not
  /// guaranteed that the previous contents of the display will have been
  /// retained on exit from this level.)
  Hibernate,

  /// The display should be put to sleep; typically this means the output
  /// is disabled/not visible (unless a persistent display technology like
  /// eInk is being used) but display contents should be preserved.
  Sleep,

  /// The display should be in a reduced power mode.  This may result in
  /// reduced brightness or refresh rate or other compromises, but the display
  /// itself should essentially be functional.
  Reduced,

  /// Normal power use.
  Normal
}


pub trait DisplayDevice : Send + Sync {
  /// The type of pixel data this display renders
  type PIXEL;

  /// Reset the device to 'power on' configuration
  fn reset(&self) -> GfxResult<()>;

  /// Request the display device moves to the indicated power level.
  fn request_power_level(&self, level: PowerLevel) -> GfxResult<()>;

  /// Render the given scene in its entirety, replacing whatever is on the
  /// screen already
  fn render<RPIXEL,RENDERABLE>(&self, scene: &RENDERABLE) -> GfxResult<()>
  where
    RPIXEL: Into<Self::PIXEL>,
    RENDERABLE: Renderable<PIXEL=RPIXEL>;

  /// Render only the parts of the display that have - or may have - changed
  /// since the last time it was rendered.  The worst-case, always correct,
  /// implementation of this method will just redraw the entire display -
  /// however the driver may be able to make more efficient choices than that.
  fn render_changed<RPIXEL,RENDERABLE>(&self, scene: &RENDERABLE) -> GfxResult<()>
  where
    RPIXEL: Into<Self::PIXEL>,
    RENDERABLE: Renderable<PIXEL=RPIXEL> {
    self.render(scene)
  }
}

// Code ======================================================================

// Tests =====================================================================
