# AVRox-Storage
Display device drivers for the [AVRoxide] AVR operating system

# Supported Display Devices
* DisplayVisions EA-W096016 96x16 dot OLED display 
* Adafruit 128x128 Greyscale OLED display

# Supported Display Drivers
* Solomon Systech SSD1306B
* Solomon Systech SSD1327

# Documentation
See the main AVRoxide documentation at [https://avroxi.de/categories/gettingstarted/](https://avroxi.de/categories/gettingstarted/)


